# provisioning-myec3-symfony

## Local Dev installation

Copy `.env.dev` to `.env.dev.local` if you need to apply changes to the default dev values.

Build app : `make composer-install`

Start the app with `docker-compose up`

Create tables with `docker-compose exec app php bin/console doctrine:migrations:migrate`

## Installation

On se reportera à la documentation d'installation de Pastell afin d'installer un environnement fonctionnel.

```sql
CREATE DATABASE provisionning_myec3 character set utf8mb4 collate utf8mb4_unicode_ci;
CREATE user "provisionning"@"localhost";
SET password FOR "provisionning"@"localhost" = password('provisionning');
GRANT ALL ON provisionning_myec3.* TO "provisionning"@"localhost";
```

```bash
export VERSION=1.0.0
cd /tmp
wget http://ressources.libriciel.fr/pastell/provisionning-myec3-${VERSION}.tar.gz
tar xvzf provisionning-myec3-${VERSION}.tar.gz
mv provisionning-myec3 /var/www/provisionning-myec3-${VERSION}
ln -s /var/www/provisionning-myec3-${VERSION} /var/www/provisionning-myec3  
```

Configuration de l'application

```bash
vi /var/www/provisionning-myec3/.env.local 
```

```dotenv
APP_ENV=prod
APP_DEBUG=0
DATABASE_URL=mysql://provisionning:provisionning_myec3@localhost:3306/provisionning_myec3?serverVersion=5.7

PASTELL_URL=https://127.0.0.1:8443
PASTELL_USERNAME=admin
PASTELL_PASSWORD=admin
PASTELL_PROXY=http://user:password@ip:port
VERIFY_PEER=0
VERIFY_HOST=0

PARAPHEUR_URL=http://127.0.0.1:8080
PARAPHEUR_BASE_API=/alfresco/wcs
PARAPHEUR_USERNAME=admin
PARAPHEUR_PASSWORD=admin
PARAPHEUR_PROXY=http://user:password@ip:port

S2LOW_URL=https://127.0.0.1:10443
S2LOW_CERTIFICATE_PATH=%kernel.project_dir%/var/data/publicCert.superadmin.pem
S2LOW_KEY_PATH=%kernel.project_dir%/var/data/privateKey.superadmin.pem
S2LOW_KEY_PASSWORD=password
S2LOW_PROXY=http://user:password@ip:port
S2LOW_USER_ROLE=USER
S2LOW_AUTHORITY_GROUP_ID=1
S2LOW_AUTHORITY_DEPARTMENT=034
S2LOW_AUTHORITY_DISTRICT=3
S2LOW_AUTHORITY_TYPE_ID=55
```

Dans le vhost d'Apache : 

```apacheconfig
Alias /provisionning-myec3 /var/www/provisionning-myec3/public

 <Directory /var/www/provisionning-myec3/public>
    AllowOverride All
    Require all granted
    FallbackResource /provisionning-myec3/index.php
</Directory>
```

Pour finir : 
```bash
php /var/www/provisionning-myec3/bin/console doctrine:migrations:migrate
chmod -R 777 /var/www/provisionning-myec3/var/cache/*
```

## Test

```
curl -X POST 'http://192.168.1.15:8020/parapheur/agent' --data @../provisionning-myec3/tests/fixtures/agent.xml
```
l'utilisateur est bien créé sur le parapheur

```
curl -X PUT 'http://192.168.1.15:8020/parapheur/agent/300005473' --data @../provisionning-myec3/tests/fixtures/agent.xml
```
cela change bien les propriétés de l'utilisateur sur le parapheur

```
curl -X DELETE 'http://192.168.1.15:8020/parapheur/agent/300005473'
```
l'utilisateur est bien détruit
