# [2.2.0] - 2022-11-02

* S2low: Ajouter le siren de l'entité synchronisée au groupe lors de la création #18
* Pastell: Le certificat du connecteur s2low n'était plus fonctionnel après une mise à jour du service #19
* Pastell: Le déplacement d’un utilisateur sur un autre service ne change pas son entité de base
et ne met pas à jour ses rôles #20
* Pastell: Permettre l'association du connecteur libersign lors de la création d'une entité #21
* Parapheur monotenant: Utilisation du champ "id" à la place de "externalId" pour la synchronisation des groupes #22

# [2.1.0] - 2021-08-03

* Pastell: Synchronisation des connecteurs s2low et libersign lors de la création/modification des departments #13
* Parapheur monotenant: Synchroniser les groupes #14
* Pastell: Création de l'utilisateur technique au niveau de l'organisme à la place du department #16
* Parapheur multitenant: Création des tenants et synchronisation des agents #15

# [2.0.0] - 2021-06-09

* Intégration de la lib `provisioning-myec3` dans ce projet
* Compatibilité avec composer 2.0
* Mise à jour de Doctrine Migration de v2 vers v3
    * Spécifier la version exact de la BDD dans le paramètre `serverVersion` de la variable `DATABASE_URL` (voir `.env.dev`)
    * À exécuter :
        * `bin/console doctrine:migrations:sync-metadata-storage`
        * `INSERT INTO doctrine_migration_versions (version, executed_at, execution_time) SELECT concat("DoctrineMigrations\\Version", version), executed_at, 1 FROM migration_versions;`
* Intégration de la lib `myec3` dans ce projet
* Permettre de provisioner plusieurs instances du même produit #7
    * Exécuter : `bin/console doctrine:migrations:migrate` (après avoir fait le sync et le insert de l'étape précédente)
    * Adapter les valeurs dans le fichier d'environnement (voir .env.dev)
* Pastell: Remplacer la désactivation de l'utilisateur par la suppression #8
* Pastell: Synchroniser le changement d'organigramme d'un utilisateur #9
* S2low : Automatiser le partage des certificats des utilisateurs #10
* Pastell: Créer un utilisateur technique lors de la création du department #11
* S2low : Créer un utilisateur technique lors de la création de l'organisme #12

# 1.2.2

## Evolutions

- Les utilisateurs sont rattaché au "department" pour Pastell 

# 1.2.0 - 2020-04-22

* Gestion des `organismDepartment` pour pastell

# 1.0.0 - 2020-02-06

## Ajouts

- premier tag
