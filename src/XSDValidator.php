<?php
/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 29/08/19
 * Time: 17:21
 */

namespace App;

use DOMDocument;
use Exception;

class XSDValidator
{
    /**
     * @param string $schema The path to the schema file
     * @param string $xml The xml
     * @return bool
     * @throws Exception
     */
    public static function schemaValidate(string $schema, string $xml): bool
    {
        $tempXml = tmpfile();
        $pathXml = stream_get_meta_data($tempXml)['uri'];
        fwrite($tempXml, $xml);

        libxml_use_internal_errors(true);
        $dom = new DOMDocument();
        $dom->load($pathXml);
        $err = $dom->schemaValidate($schema);
        if (!$err) {
            $last_error = libxml_get_errors();
            $msg = ' ';
            foreach ($last_error as $err) {
                $msg .= "[Erreur #{$err->code}] " . $err->message . "\n";
            }
            fclose($tempXml);
            throw new Exception($msg);
        }
        fclose($tempXml);

        return true;
    }
}
