<?php

namespace App\Service;

use App\Http\Client\S2lowClient;
use App\Http\Client\S2lowClientException;
use App\Http\Model\S2lowUser;
use Psr\Http\Client\ClientExceptionInterface;

class S2lowTechnicalUserService
{
    /**
     * @var S2lowClient
     */
    private $s2lowClient;
    /**
     * @var HashService
     */
    private $hashService;
    /**
     * @var array $userValues : List of values required to create the user (email, certificate, role...)
     */
    private $userValues;

    public function __construct(
        S2lowClient $s2lowClient,
        HashService $hashService,
        array $values
    ) {
        $this->s2lowClient = $s2lowClient;
        $this->hashService = $hashService;
        $this->userValues = $values;
    }

    /**
     * @throws S2lowClientException
     * @throws ClientExceptionInterface
     */
    public function create(string $organismSocleId, int $entityId): void
    {
        $user = new S2lowUser();
        $user->login = 'tech_' . $organismSocleId;
        $user->setPassword($this->hashService->getHash($organismSocleId));
        $user->name = 'Tech';
        $user->givenname = 'User';
        $user->email = $this->userValues['email'];
        $user->certificate = $this->userValues['certificate'];
        $user->status = 1;
        $user->role = $this->userValues['role'];
        $user->authority_group_id = $this->userValues['authority_group_id'];
        $user->authority_id = $entityId;
        $user->auth_method = 2;
        $user->setActPermission('RW');
        $user->setHeliosPermission('RW');

        $response = $this->s2lowClient->createOrUpdateUser($user);
        $userId = $response['id'];

        $services = $this->s2lowClient->getAuthorityServices($entityId);
        if (!empty($services)) {
            $this->s2lowClient->addUserToService($userId, $services[0]['id']);
        }
    }
}
