<?php

namespace App\Service;

class HashService
{
    public const DEFAULT_ALGO = 'sha256';

    /**
     * @var string
     */
    private $key;

    /** @var string */
    private $algo;

    public function __construct(string $key)
    {
        $this->key = $key;
        $this->setAlgo(self::DEFAULT_ALGO);
    }

    public function getHash(string $message): string
    {
        return hash_hmac($this->algo, $message, $this->key);
    }

    public function getAlgo(): string
    {
        return $this->algo;
    }

    public function setAlgo(string $algo): HashService
    {
        if (in_array($algo, hash_hmac_algos())) {
            $this->algo = $algo;
        } else {
            $this->algo = self::DEFAULT_ALGO;
        }
        return $this;
    }
}
