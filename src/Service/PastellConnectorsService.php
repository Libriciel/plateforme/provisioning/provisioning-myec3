<?php

namespace App\Service;

use PastellClient\Api\ConnectorsRequester;
use PastellClient\Api\ModuleAssociationsRequester;
use PastellClient\Model\Connector;
use Psr\Http\Client\ClientExceptionInterface;

class PastellConnectorsService
{
    /** @var string */
    public const LIBERSIGN = 'libersign';
    /** @var string */
    public const S2LOW = 's2low';

    /**
     * @var ConnectorsRequester
     */
    private $connectorsRequester;
    /**
     * @var ModuleAssociationsRequester
     */
    private $moduleAssociationsRequester;
    /**
     * @var HashService
     */
    private $hashService;
    /**
     * @var array
     */
    private $connectorsData;

    public function __construct(
        ConnectorsRequester $connectorsRequester,
        ModuleAssociationsRequester $moduleAssociationsRequester,
        HashService $hashService,
        array $connectorsData
    ) {
        $this->connectorsRequester = $connectorsRequester;
        $this->moduleAssociationsRequester = $moduleAssociationsRequester;
        $this->hashService = $hashService;
        $this->connectorsData = $connectorsData;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function saveTdTConnector(int $entityId, string $organismId): void
    {
        $connector = $this->saveConnector(
            $entityId,
            self::S2LOW,
            $this->connectorsData[self::S2LOW]['label']
        );

        $certificatePath = $this->connectorsData[self::S2LOW]['user_certificat']['filepath'];
        $connector = $this->connectorsRequester->uploadFile(
            $connector,
            'user_certificat',
            $certificatePath,
            basename($certificatePath)
        );

        $connector->data->data['url'] = $this->connectorsData[self::S2LOW]['url'];
        $connector->data->data['user_certificat_password'] = $this->connectorsData[self::S2LOW]['user_certificat']['password'];
        $connector->data->data['user_login'] = 'tech_' . $organismId;
        $connector->data->data['user_password'] = $this->hashService->getHash($organismId);
        $connector->data->data['authentication_for_teletransmisson'] = $this->connectorsData[self::S2LOW]['authentication_for_teletransmisson'];

        $connector = $this->connectorsRequester->update($connector);

        foreach ($this->connectorsData[self::S2LOW]['associations'] as $association) {
            $this->associateConnector($connector, $association);
        }
        $this->connectorsRequester->triggerAction($connector, 'recup-classification');
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function saveLibersignConnector(int $entityId): Connector
    {
        $connector = $this->saveConnector(
            $entityId,
            self::LIBERSIGN,
            $this->connectorsData[self::LIBERSIGN]['label']
        );

        $connector->data->data['libersign_applet_url'] = $this->connectorsData[self::LIBERSIGN]['libersign_applet_url'];
        $connector->data->data['libersign_extension_update_url'] = $this->connectorsData[self::LIBERSIGN]['libersign_extension_update_url'];
        $connector->data->data['libersign_help_url'] = $this->connectorsData[self::LIBERSIGN]['libersign_help_url'];
        $connector->data->data['libersign_xmlstarlet_path'] = $this->connectorsData[self::LIBERSIGN]['libersign_xmlstarlet_path'];

        return $this->connectorsRequester->update($connector);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function associateLibersignConnector(Connector $connector): void
    {
        foreach ($this->connectorsData[self::LIBERSIGN]['associations'] as $association) {
            $this->associateConnector($connector, $association);
        }
    }

    /**
     * @throws ClientExceptionInterface
     */
    private function saveConnector(int $entityId, string $type, string $connectorLabel): Connector
    {
        $connectors = $this->connectorsRequester->all($entityId);
        $saved = null;
        foreach ($connectors as $connector) {
            if ($connector->getIdConnecteur() === $type && $connector->getLibelle() === $connectorLabel) {
                $saved = $connector;
            }
        }
        return $saved ?? $this->connectorsRequester->create($entityId, $type, $connectorLabel);
    }

    /**
     * @throws ClientExceptionInterface
     */
    private function associateConnector(Connector $connector, string $module): void
    {
        $associations = $this->moduleAssociationsRequester->all($connector->getIdE(), $module, $connector->getType());
        if (empty($associations) || $associations[0]['id_ce'] != $connector->getIdCe()) {
            $this->moduleAssociationsRequester->associate(
                $connector->getIdE(),
                $connector->getIdCe(),
                $module,
                $connector->getType()
            );
        }
    }
}
