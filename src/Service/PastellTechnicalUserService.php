<?php

namespace App\Service;

use PastellClient\Api\UserRolesRequester;
use PastellClient\Api\UsersRequester;
use PastellClient\Exception\PastellException;
use PastellClient\Hydrator\UserHydrator;
use PastellClient\Model\UserRole;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class PastellTechnicalUserService
{
    /**
     * @var UserRolesRequester
     */
    private $userRolesRequester;
    /**
     * @var UsersRequester
     */
    private $usersRequester;
    /**
     * @var HashService
     */
    private $hashService;
    /**
     * @var UserHydrator
     */
    private $userHydrator;
    /**
     * @var string
     */
    private $email;
    /**
     * @var array
     */
    private $roles;

    public function __construct(
        UsersRequester $usersRequester,
        UserRolesRequester $userRolesRequester,
        HashService $hashService,
        string $email,
        array $roles,
        UserHydrator $userHydrator = null
    ) {
        $this->userRolesRequester = $userRolesRequester;
        $this->usersRequester = $usersRequester;
        $this->hashService = $hashService;
        $this->email = $email;
        $this->roles = $roles;
        $this->userHydrator = $userHydrator ?? new UserHydrator();
    }

    /**
     * @throws ClientExceptionInterface
     * @throws PastellException
     */
    public function create(SimpleXMLElement $xml, int $entityId): void
    {
        $user = [
            'login' => 'tech_' . $xml->id,
            'password' => $this->hashService->getHash($xml->id),
            'prenom' => 'User',
            'nom' => 'Tech',
            'email' => $this->email,
            'id_e' => $entityId
        ];

        try {
            $response = $this->usersRequester->create($this->userHydrator->hydrate($user));
            $userId = $response->getId();
        } catch (PastellException $exception) {
            $message = json_decode($exception->getMessage(), true);
            if (!empty($message['error-message']) && $message['error-message'] === 'Ce login existe déjà') {
                $userId = $this->getUserIdByLogin($entityId, $user['login']);
                if (is_null($userId)) {
                    throw $exception;
                }
            }
        }

        foreach ($this->roles as $role) {
            $userRole = new UserRole();
            $userRole->id_u = $userId;
            $userRole->id_e = $entityId;
            $userRole->role = $role;
            $this->userRolesRequester->add($userRole);
        }
    }

    /**
     * @throws ClientExceptionInterface
     */
    private function getUserIdByLogin(int $entityId, string $login): ?int
    {
        $userId = null;

        $users = $this->usersRequester->all(['id_e' => $entityId]);
        foreach ($users as $currentUser) {
            if ($currentUser->login === $login) {
                $userId = $currentUser->getId();
            }
        }
        return $userId;
    }
}
