<?php

namespace App\Command;

use Symfony\Component\Console\Input\InputOption;

class ImportUsersCommand extends BaseImportCommand
{
    protected static $defaultName = 'app:import-users';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDefinition([
                new InputOption(self::TABLE_NAME, null, InputOption::VALUE_REQUIRED,
                    'Name of the table which contains the users'),
                new InputOption(self::PRODUCT_NAME, null, InputOption::VALUE_REQUIRED,
                    'Name of the product (pastell|parapheur|s2low)'),
                new InputOption(self::SOCLE_ID, null, InputOption::VALUE_REQUIRED,
                    'Name of the column which contains the id of the user in the "socle"'),
                new InputOption(self::PRODUCT_ID, null, InputOption::VALUE_REQUIRED,
                    'Name of the column which contains the id of the user in the product'),
            ])
            ->setDescription('Import users from the socle.');
    }


    public function getTableName(): string
    {
        return 'product_user_socle';
    }

    public function getProductNameColumn(): string
    {
        return 'product_id';
    }

    public function getSocleIdColumn(): string
    {
        return 'user_socle_id';
    }

    public function getProductIdColumn(): string
    {
        return 'user_product_id';
    }
}
