<?php

namespace App\Command;

use Symfony\Component\Console\Input\InputOption;

class ImportOrganizationsCommand extends BaseImportCommand
{
    protected static $defaultName = 'app:import-organizations';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDefinition([
                new InputOption(self::TABLE_NAME, null, InputOption::VALUE_REQUIRED,
                    'Name of the table which contains the organizations'),
                new InputOption(self::PRODUCT_NAME, null, InputOption::VALUE_REQUIRED,
                    'Name of the product (pastell|parapheur|s2low)'),
                new InputOption(self::SOCLE_ID, null, InputOption::VALUE_REQUIRED,
                    'Name of the column which contains the id of the organization in the "socle"'),
                new InputOption(self::PRODUCT_ID, null, InputOption::VALUE_REQUIRED,
                    'Name of the column which contains the id of the organization in the product'),
            ])
            ->setDescription('Import organizations from the socle.');
    }


    public function getTableName(): string
    {
        return 'product_organization_socle';
    }

    public function getProductNameColumn(): string
    {
        return 'product_id';
    }

    public function getSocleIdColumn(): string
    {
        return 'organization_socle_id';
    }

    public function getProductIdColumn(): string
    {
        return 'organization_product_id';
    }
}
