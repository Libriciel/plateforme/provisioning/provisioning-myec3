<?php

namespace App\Command;

use Symfony\Component\Console\Input\InputOption;

class ImportDepartmentsCommand extends BaseImportCommand
{
    protected static $defaultName = 'app:import-departments';

    /**
     * {@inheritdoc}
     */
    protected function configure()
    {
        $this
            ->setDefinition([
                new InputOption(self::TABLE_NAME, null, InputOption::VALUE_REQUIRED,
                    'Name of the table which contains the departments'),
                new InputOption(self::PRODUCT_NAME, null, InputOption::VALUE_REQUIRED,
                    'Name of the product (pastell)'),
                new InputOption(self::SOCLE_ID, null, InputOption::VALUE_REQUIRED,
                    'Name of the column which contains the id of the department in the "socle"'),
                new InputOption(self::PRODUCT_ID, null, InputOption::VALUE_REQUIRED,
                    'Name of the column which contains the id of the department in the product'),
            ])
            ->setDescription('Import departments from the socle.');
    }
    public function getTableName(): string
    {
        return 'product_department_socle';
    }

    public function getProductNameColumn(): string
    {
        return 'product_id';
    }

    public function getSocleIdColumn(): string
    {
        return 'department_socle_id';
    }

    public function getProductIdColumn(): string
    {
        return 'department_product_id';
    }
}
