<?php

namespace App\Command;

use Doctrine\DBAL\Connection;
use Doctrine\DBAL\DBALException;
use Doctrine\DBAL\Schema\Column;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class BaseImportCommand extends Command
{
    protected const TABLE_NAME = 'table_name';
    protected const PRODUCT_NAME = 'product_name';
    protected const SOCLE_ID = 'socle_id';
    protected const PRODUCT_ID = 'product_id';

    /**
     * @var Connection
     */
    private $connection;

    /**
     * @var SymfonyStyle
     */
    private $io;

    public function __construct(Connection $connection)
    {
        parent::__construct();

        $this->connection = $connection;
    }

    /**
     * {@inheritdoc}
     */
    public function initialize(InputInterface $input, OutputInterface $output)
    {
        $this->io = new SymfonyStyle($input, $output);
    }

    /**
     * {@inheritdoc}
     */
    protected function interact(InputInterface $input, OutputInterface $output)
    {
        $this->io->title($this->getDescription());

        $this->askQuestion(
            $input,
            self::TABLE_NAME,
            $this->connection->getSchemaManager()->listTableNames()
        );

        $this->askQuestion(
            $input,
            self::PRODUCT_NAME,
            ['pastell', 'iparapheur', 's2low']
        );

        $columns = $this->connection->getSchemaManager()->listTableColumns($input->getOption(self::TABLE_NAME));
        $columnsName = array_map(function (Column $col) {
            return $col->getName();
        }, $columns);

        $this->askQuestion(
            $input,
            self::SOCLE_ID,
            array_values($columnsName)
        );
        $this->askQuestion(
            $input,
            self::PRODUCT_ID,
            array_values($columnsName)
        );
    }

    /**
     * @throws DBALException
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $tableName = $input->getOption(self::TABLE_NAME);
        $productName = $input->getOption(self::PRODUCT_NAME);
        $socleId = $input->getOption(self::SOCLE_ID);
        $productId = $input->getOption(self::PRODUCT_ID);


        $sql = sprintf(
            "SELECT %s,%s FROM %s;",
            $socleId,
            $productId,
            $tableName
        );
        $statement = $this->connection->executeQuery($sql);

        $this->io->progressStart($this->connection->executeQuery("SELECT COUNT(*) as nb FROM $tableName;")->fetch()['nb']);
        while ($row = $statement->fetch()) {
            $this->connection->insert($this->getTableName(), [
                $this->getProductNameColumn() => $productName,
                $this->getProductIdColumn() => $row[$productId],
                $this->getSocleIdColumn() => $row[$socleId]
            ]);
            $this->io->progressAdvance();
        }
        $this->io->progressFinish();
        $this->io->writeln('');
        $this->io->success('Done');
        return 0;
    }

    private function askQuestion(
        InputInterface $input,
        string $option,
        array $choices
    ): void {
        if (!$input->getOption($option)) {
            $question = new ChoiceQuestion(
                $this->getDefinition()->getOption($option)->getDescription(),
                $choices
            );
            $answer = $this->io->askQuestion($question);
            $input->setOption($option, $answer);
        }
    }

    abstract public function getTableName(): string;

    abstract public function getProductNameColumn(): string;

    abstract public function getSocleIdColumn(): string;

    abstract public function getProductIdColumn(): string;
}
