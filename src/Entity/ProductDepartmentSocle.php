<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductDepartmentSocleRepository")
 */
class ProductDepartmentSocle
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private $product_id;

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private $department_socle_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $department_product_id;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $product_number;

    public function getProductId(): ?string
    {
        return $this->product_id;
    }

    public function setProductId(string $product_id): self
    {
        $this->product_id = $product_id;

        return $this;
    }

    public function getDepartmentSocleId(): ?string
    {
        return $this->department_socle_id;
    }

    public function setDepartmentSocleId(string $department_socle_id): self
    {
        $this->department_socle_id = $department_socle_id;

        return $this;
    }

    public function getDepartmentProductId(): ?string
    {
        return $this->department_product_id;
    }

    public function setDepartmentProductId(string $department_product_id): self
    {
        $this->department_product_id = $department_product_id;

        return $this;
    }

    public function getProductNumber(): ?int
    {
        return $this->product_number;
    }

    public function setProductNumber(int $product_number): self
    {
        $this->product_number = $product_number;

        return $this;
    }
}
