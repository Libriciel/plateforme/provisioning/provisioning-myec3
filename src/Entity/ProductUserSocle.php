<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductUserSocleRepository")
 */
class ProductUserSocle
{

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private $product_id;

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private $user_socle_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $user_product_id;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $product_number;

    public function getProductId(): ?string
    {
        return $this->product_id;
    }

    public function setProductId(string $product_id): self
    {
        $this->product_id = $product_id;

        return $this;
    }

    public function getUserSocleId(): ?string
    {
        return $this->user_socle_id;
    }

    public function setUserSocleId(string $user_socle_id): self
    {
        $this->user_socle_id = $user_socle_id;

        return $this;
    }

    public function getUserProductId(): ?string
    {
        return $this->user_product_id;
    }

    public function setUserProductId(string $user_product_id): self
    {
        $this->user_product_id = $user_product_id;

        return $this;
    }

    public function getProductNumber(): ?int
    {
        return $this->product_number;
    }

    public function setProductNumber(int $product_number): self
    {
        $this->product_number = $product_number;

        return $this;
    }
}
