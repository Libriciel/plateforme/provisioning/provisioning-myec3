<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProductOrganizationSocleRepository")
 */
class ProductOrganizationSocle
{
    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private $product_id;

    /**
     * @ORM\Id()
     * @ORM\Column(type="string", length=255)
     */
    private $organization_socle_id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $organization_product_id;

    /**
     * @ORM\Id()
     * @ORM\Column(type="integer")
     */
    private $product_number;

    public function getProductId(): ?string
    {
        return $this->product_id;
    }

    public function setProductId(string $product_id): self
    {
        $this->product_id = $product_id;

        return $this;
    }

    public function getOrganizationSocleId(): ?string
    {
        return $this->organization_socle_id;
    }

    public function setOrganizationSocleId(string $organization_socle_id): self
    {
        $this->organization_socle_id = $organization_socle_id;

        return $this;
    }

    public function getOrganizationProductId(): ?string
    {
        return $this->organization_product_id;
    }

    public function setOrganizationProductId(string $organization_product_id): self
    {
        $this->organization_product_id = $organization_product_id;

        return $this;
    }

    public function getProductNumber(): ?int
    {
        return $this->product_number;
    }

    public function setProductNumber(int $product_number): self
    {
        $this->product_number = $product_number;

        return $this;
    }
}
