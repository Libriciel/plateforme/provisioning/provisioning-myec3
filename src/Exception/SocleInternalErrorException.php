<?php
/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 10/10/19
 * Time: 11:49
 */

namespace App\Exception;

/**
 * Exception raised when a particular record was not found
 */
class SocleInternalErrorException extends SocleException
{
    /**
     * SocleAlreadyExitsRessourceException.
     * @param string $resource resource
     * @param string $resourceId resourceId
     * @param string $methodType method
     */
    public function __construct($resource, $resourceId, $methodType)
    {
        parent::__construct(
            $resource,
            '006',
            'INTERNAL_SERVER_ERROR',
            'Erreur interne au serveur',
            $methodType,
            $resourceId,
            500,
            null
        );
    }
}
