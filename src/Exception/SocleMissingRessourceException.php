<?php
/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 10/10/19
 * Time: 11:49
 */

namespace App\Exception;

/**
 * Exception raised when a particular record was not found
 */
class SocleMissingRessourceException extends SocleException
{
    /**
     * SocleMissingRessourceException constructor.
     * @param string $resource resource
     * @param string $resourceId resourceId
     * @param string $methodType method
     */
    public function __construct($resource, $resourceId, $methodType)
    {
        parent::__construct(
            $resource,
            '003',
            'RESOURCE_MISSING',
            'La ressource spécifiée est introuvable',
            $methodType,
            $resourceId,
            404,
            null
        );
    }
}
