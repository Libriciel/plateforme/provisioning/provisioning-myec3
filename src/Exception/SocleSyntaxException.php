<?php
/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 10/10/19
 * Time: 11:49
 */

namespace App\Exception;

/**
 * Exception raised when a particular record was not found
 */
class SocleSyntaxException extends SocleException
{
    /**
     * SocleSyntaxException.
     */
    public function __construct()
    {
        parent::__construct(
            'unknown',
            '001',
            'SYNTAX_ERROR',
            'Syntaxe du message en entrée incorrecte',
            'unknown',
            'unknown',
            400,
            null
        );
    }
}
