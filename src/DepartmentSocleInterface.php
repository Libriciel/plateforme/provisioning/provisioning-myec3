<?php


namespace App;


use SimpleXMLElement;

interface DepartmentSocleInterface
{
    /**
     * get department by externalId
     * @param string $externalId
     * @return string|int
     */
    public function getId(string $externalId);

    /**
     * Add structure
     * @param SimpleXMLElement $xml
     * @return bool
     */
    public function add(SimpleXMLElement $xml): bool;

    /**
     * update a department
     * @param SimpleXMLElement $xml
     * @param string|int $departmentId
     * @return bool
     */
    public function update(SimpleXMLElement $xml, $departmentId): bool;

    /**
     * @param string|int $departmentId
     * @return bool
     */
    public function delete($departmentId): bool;

}
