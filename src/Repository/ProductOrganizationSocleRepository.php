<?php

namespace App\Repository;

use App\Entity\ProductOrganizationSocle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductOrganizationSocle|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductOrganizationSocle|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductOrganizationSocle[]    findAll()
 * @method ProductOrganizationSocle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductOrganizationSocleRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, ProductOrganizationSocle::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function get(
        string $productId,
        int $productNumber,
        string $organizationSocleId
    ): ?ProductOrganizationSocle {
        return $this->createQueryBuilder('p')
            ->andWhere('p.product_id = :product_id')
            ->andWhere('p.product_number = :product_number')
            ->andWhere('p.organization_socle_id = :organization_socle_id')
            ->setParameter('product_id', $productId)
            ->setParameter('product_number', $productNumber)
            ->setParameter('organization_socle_id', $organizationSocleId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getByOrganizationProductId(
        string $productId,
        int $productNumber,
        string $organizationProductId
    ): ?ProductOrganizationSocle {
        return $this->createQueryBuilder('p')
            ->andWhere('p.product_id = :product_id')
            ->andWhere('p.product_number = :product_number')
            ->andWhere('p.organization_product_id = :organization_product_id')
            ->setParameter('product_id', $productId)
            ->setParameter('product_number', $productNumber)
            ->setParameter('organization_product_id', $organizationProductId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function exists(string $productId, int $productNumber, string $organizationSocleId): bool
    {
        return (bool)$this->createQueryBuilder('p')
            ->andWhere('p.product_id = :product_id')
            ->andWhere('p.product_number = :product_number')
            ->andWhere('p.organization_socle_id = :organization_socle_id')
            ->setParameter('product_id', $productId)
            ->setParameter('product_number', $productNumber)
            ->setParameter('organization_socle_id', $organizationSocleId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function add(ProductOrganizationSocle $productOrganizationSocle): bool
    {
        try {
            $this->_em->persist($productOrganizationSocle);
            $this->_em->flush();
        } catch (ORMException $e) {
            return false;
        }
        return true;
    }

    public function delete(ProductOrganizationSocle $productOrganizationSocle): bool
    {
        try {
            $attachedEntity = $this->_em->merge($productOrganizationSocle);
            $this->_em->remove($attachedEntity);
            $this->_em->flush();
        } catch (ORMException $e) {
            return false;
        }
        return true;
    }
}
