<?php

namespace App\Repository;

use App\Entity\ProductUserSocle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductUserSocle|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductUserSocle|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductUserSocle[]    findAll()
 * @method ProductUserSocle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductUserSocleRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductUserSocle::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function get(string $productId, int $productNumber, string $userSocleId): ?ProductUserSocle
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.product_id = :product_id')
            ->andWhere('p.product_number = :product_number')
            ->andWhere('p.user_socle_id = :user_socle_id')
            ->setParameter('product_id', $productId)
            ->setParameter('product_number', $productNumber)
            ->setParameter('user_socle_id', $userSocleId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getByUserProductId(
        string $productId,
        int $productNumber,
        string $userProductId
    ): ?ProductUserSocle {
        return $this->createQueryBuilder('p')
            ->andWhere('p.product_id = :product_id')
            ->andWhere('p.product_number = :product_number')
            ->andWhere('p.user_product_id = :user_product_id')
            ->setParameter('product_id', $productId)
            ->setParameter('product_number', $productNumber)
            ->setParameter('user_product_id', $userProductId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function exists(string $productId, int $productNumber, string $userSocleId): bool
    {
        return (bool)$this->createQueryBuilder('p')
            ->andWhere('p.product_id = :product_id')
            ->andWhere('p.product_number = :product_number')
            ->andWhere('p.user_socle_id = :user_socle_id')
            ->setParameter('product_id', $productId)
            ->setParameter('product_number', $productNumber)
            ->setParameter('user_socle_id', $userSocleId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function add(ProductUserSocle $productUserSocle): bool
    {
        try {
            $this->_em->persist($productUserSocle);
            $this->_em->flush();
        } catch (ORMException $e) {
            return false;
        }
        return true;
    }

    public function delete(ProductUserSocle $productUserSocle): bool
    {
        try {
            $attachedEntity = $this->_em->merge($productUserSocle);
            $this->_em->remove($attachedEntity);
            $this->_em->flush();
        } catch (ORMException $e) {
            return false;
        }
        return true;
    }
}
