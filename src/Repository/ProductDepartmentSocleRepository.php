<?php

namespace App\Repository;

use App\Entity\ProductDepartmentSocle;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method ProductDepartmentSocle|null find($id, $lockMode = null, $lockVersion = null)
 * @method ProductDepartmentSocle|null findOneBy(array $criteria, array $orderBy = null)
 * @method ProductDepartmentSocle[]    findAll()
 * @method ProductDepartmentSocle[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductDepartmentSocleRepository extends ServiceEntityRepository
{

    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ProductDepartmentSocle::class);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function get(string $productId, int $productNumber, string $departmentSocleId): ?ProductDepartmentSocle
    {
        return $this->createQueryBuilder('p')
            ->andWhere('p.product_id = :product_id')
            ->andWhere('p.product_number = :product_number')
            ->andWhere('p.department_socle_id = :department_socle_id')
            ->setParameter('product_id', $productId)
            ->setParameter('product_number', $productNumber)
            ->setParameter('department_socle_id', $departmentSocleId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getByDepartmentProductId(
        string $productId,
        int $productNumber,
        string $departmentProductId
    ): ?ProductDepartmentSocle {
        return $this->createQueryBuilder('p')
            ->andWhere('p.product_id = :product_id')
            ->andWhere('p.product_number = :product_number')
            ->andWhere('p.department_product_id = :department_product_id')
            ->setParameter('product_id', $productId)
            ->setParameter('product_number', $productNumber)
            ->setParameter('department_product_id', $departmentProductId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    /**
     * @throws NonUniqueResultException
     */
    public function exists(string $productId, int $productNumber, string $departmentSocleId): bool
    {
        return (bool)$this->createQueryBuilder('p')
            ->andWhere('p.product_id = :product_id')
            ->andWhere('p.product_number = :product_number')
            ->andWhere('p.department_socle_id = :department_socle_id')
            ->setParameter('product_id', $productId)
            ->setParameter('product_number', $productNumber)
            ->setParameter('department_socle_id', $departmentSocleId)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function add(ProductDepartmentSocle $productDepartmentSocle): bool
    {
        try {
            $this->_em->persist($productDepartmentSocle);
            $this->_em->flush();
        } catch (ORMException $e) {
            return false;
        }
        return true;
    }

    public function delete(ProductDepartmentSocle $productDepartmentSocle): bool
    {
        try {
            $attachedEntity = $this->_em->merge($productDepartmentSocle);
            $this->_em->remove($attachedEntity);
            $this->_em->flush();
        } catch (ORMException $e) {
            return false;
        }
        return true;
    }
}
