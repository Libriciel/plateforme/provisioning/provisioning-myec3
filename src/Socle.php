<?php
/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 09/10/19
 * Time: 10:35
 */

namespace App;

use App\Exception\SocleAlreadyExitsRessourceException;
use App\Exception\SocleException;
use App\Exception\SocleInternalErrorException;
use App\Exception\SocleMissingRessourceException;
use App\Exception\SocleRelationMissingException;
use App\Exception\SocleSyntaxException;
use SimpleXMLElement;

class Socle
{
    const XSD_SOCLE = __DIR__ . "/schema.xsd";
    const XSD_SOCLE_ERROR = __DIR__ . "/error.xsd";

    /**
     * @var UserSocleInterface $User
     */
    protected $Users;

    /**
     * @var StructureSocleInterface $Structures
     */
    protected $Structures;

    /**
     * @var DepartmentSocleInterface $Department
     */
    protected $Department;

    public function __construct(
        UserSocleInterface $Users = null,
        StructureSocleInterface $Structures = null,
        DepartmentSocleInterface $Department = null
    ) {
        $this->Users = $Users;
        $this->Structures = $Structures;
        $this->Department = $Department;
    }

    /**
     * Add a structure
     * @param string $xmlString xml
     * @return mixed
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleSyntaxException
     * @throws SocleInternalErrorException
     */
    public function addStructureXml($xmlString)
    {
        $this->validateXml($xmlString);
        /**
         * @var $xml SimpleXMLElement
         */
        $xml = new SimpleXMLElement($xmlString);
        $externalId = strval($xml->externalId);

        //check if $structure already exists
        $structureId = $this->Structures->getId($externalId);
        if (!empty($structureId)) {
            throw new SocleAlreadyExitsRessourceException(SocleException::ORGANISM, $externalId);
        }

        $res = $this->Structures->add($xml);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::ORGANISM,
                $externalId,
                SocleException::PUT
            );
        }
        return $res;
    }

    /**
     * update a structure
     * @param string $xmlString xml
     * @param string $externalId externalId
     * @return mixed
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleInternalErrorException
     * @throws SocleSyntaxException
     */
    public function updateStructureXml($xmlString, $externalId)
    {
        $this->validateXml($xmlString);
        $xml = new SimpleXMLElement($xmlString);

        //check if $structure already exists
        $structureId = $this->Structures->getId($externalId);

        if (empty($structureId)) {
            return $this->addStructureXml($xmlString);
        }

        $res = $this->Structures->update($xml, $structureId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::ORGANISM,
                $externalId,
                SocleException::PUT
            );
        }

        return $res;
    }


    /**
     * delete a structure : On ne fait que la désactiver
     * @param string $externalId externalId
     * @return mixed
     * @throws SocleMissingRessourceException
     * @throws SocleInternalErrorException
     */
    public function deleteStructure($externalId)
    {
        $structureId = $this->Structures->getId($externalId);

        if (empty($structureId)) {
            throw new SocleMissingRessourceException(
                SocleException::ORGANISM,
                $externalId,
                SocleException::DELETE
            );
        }
        $res = $this->Structures->delete($structureId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::ORGANISM,
                $externalId,
                SocleException::DELETE
            );
        }

        return $res;
    }

    /**
     * Add a structure
     * @param string $xmlString xml
     * @return mixed
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleSyntaxException
     * @throws SocleInternalErrorException
     * @throws SocleRelationMissingException
     */
    public function addDepartmentXml($xmlString)
    {
        $this->validateXml($xmlString);
        /**
         * @var $xml SimpleXMLElement
         */
        $xml = new SimpleXMLElement($xmlString);
        $externalId = strval($xml->externalId);

        //check if $structure already exists
        $departmentId = $this->Department->getId($externalId);
        if (!empty($departmentId)) {
            throw new SocleAlreadyExitsRessourceException(SocleException::DEPARTMENT, $externalId);
        }

        //check if $organism exists
        $structureExternalId = strval($xml->organism->externalId);
        if (empty($this->Structures->getId($structureExternalId))){
            throw new SocleRelationMissingException(
                SocleException::ORGANISM,
                $structureExternalId,
                'POST'
            );
        }

        //check if $parentDepartment exists
        if (strval($xml->rootDepartment) == 'false'){
            $parentDepartmentId = strval($xml->parentDepartment->externalId);
            if (! $this->Department->getId($parentDepartmentId)){
                throw new SocleRelationMissingException(
                    SocleException::DEPARTMENT,
                    $parentDepartmentId,
                    'POST'
                );
            }
        }

        $res = $this->Department->add($xml);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::DEPARTMENT,
                $externalId,
                SocleException::PUT
            );
        }
        return $res;
    }

    /**
     * update a structure
     * @param string $xmlString xml
     * @param string $externalId externalId
     * @return mixed
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleInternalErrorException
     * @throws SocleSyntaxException
     */
    public function updateDepartmentXml($xmlString, $externalId)
    {
        $this->validateXml($xmlString);
        $xml = new SimpleXMLElement($xmlString);

        //check if $department already exists
        $departmentId = $this->Department->getId($externalId);

        if (empty($departmentId)) {
            return $this->addDepartmentXml($xmlString);
        }

        $res = $this->Department->update($xml, $departmentId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::DEPARTMENT,
                $externalId,
                SocleException::PUT
            );
        }

        return $res;
    }

    /**
     * delete a structure : On ne fait que la désactiver
     * @param string $externalId externalId
     * @return mixed
     * @throws SocleMissingRessourceException
     * @throws SocleInternalErrorException
     */
    public function deleteDepartment($externalId)
    {
        $departmentId = $this->Department->getId($externalId);

        if (empty($departmentId)) {
            throw new SocleMissingRessourceException(
                SocleException::DEPARTMENT,
                $externalId,
                SocleException::DELETE
            );
        }
        $res = $this->Department->delete($departmentId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::DEPARTMENT,
                $externalId,
                SocleException::DELETE
            );
        }

        return $res;
    }


    /**
     * Ajoute un utilisateur
     * @param string $xmlString xml
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleRelationMissingException
     * @throws SocleSyntaxException
     * @throws SocleInternalErrorException
     * @return mixed
     */
    public function addUserXml($xmlString)
    {
        $this->validateXml($xmlString);
        $xml = new SimpleXMLElement($xmlString);
        $externalId = strval($xml->externalId);
        $roleName = strval($xml->roles->role->name);

        $userId = $this->Users->getId($externalId);

        if (!empty($userId)) {
            throw new SocleAlreadyExitsRessourceException(SocleException::AGENT, $externalId);
        }

        $structureExternalId = strval($xml->organismDepartment->organism->externalId);
        $structureId = $this->Structures->getId($structureExternalId);

        if (empty($structureId)) {
            throw new SocleRelationMissingException(
                SocleException::ORGANISM,
                $structureExternalId,
                SocleException::POST
            );
        }

        $roleId = $this->Structures->getRoleId($roleName, $structureId);

        if (empty($roleId)) {
            throw new SocleRelationMissingException(
                SocleException::AGENT,
                $externalId,
                SocleException::POST
            );
        }

        $res = $this->Users->add($xml, $structureId, $roleId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::AGENT,
                $externalId,
                SocleException::POST
            );
        }

        return $res;
    }

    /**
     * update an user
     * @param string $xmlString xml
     * @param string $externalId externalId
     * @throws SocleRelationMissingException
     * @throws SocleInternalErrorException
     * @throws SocleSyntaxException
     * @throws SocleAlreadyExitsRessourceException
     * @throws \Exception
     */
    public function updateUserXml($xmlString, $externalId)
    {
        $this->validateXml($xmlString);
        $xml = new SimpleXMLElement($xmlString);

        $roleName = (string)($xml->roles->role->name);

        $userId = $this->Users->getId($externalId);

        if (empty($userId)) {
            return $this->addUserXml($xmlString);
        }

        $structureExternalId = (string)$xml->organismDepartment->organism->externalId;
        $structureId = $this->Structures->getId($structureExternalId);
        $roleId = $this->Structures->getRoleId($roleName, $structureId);
        $departmentId = $this->Department->getId((string)$xml->organismDepartment->externalId);

        if (empty($roleId)) {
            throw new SocleRelationMissingException(
                SocleException::AGENT,
                $externalId,
                SocleException::PUT
            );
        }

        $res = $this->Users->update($xml, $userId, $structureId, $roleId, $departmentId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::AGENT,
                $externalId,
                SocleException::PUT
            );
        }

        return $res;
    }

    /**
     * delete an user : Par choix on ne fait que desactiver l'utilisateur !
     * @param string $externalId externalId
     * @throws SocleMissingRessourceException
     * @throws SocleInternalErrorException
     * @return mixed
     */
    public function deleteUser($externalId)
    {
        $userId = $this->Users->getId($externalId);

        if (empty($userId)) {
            throw new SocleMissingRessourceException(
                SocleException::AGENT,
                $externalId,
                SocleException::DELETE
            );
        }

        $res = $this->Users->delete($userId);

        if (empty($res)) {
            throw new SocleInternalErrorException(
                SocleException::AGENT,
                $externalId,
                SocleException::DELETE
            );
        }

        return $res;
    }

    /**
     * @throws SocleSyntaxException
     */
    protected function validateXml(string $xml): void
    {
        try {
            XSDValidator::schemaValidate(self::XSD_SOCLE, $xml);
        } catch (\Exception $e) {
            throw new SocleSyntaxException();
        }
    }
}
