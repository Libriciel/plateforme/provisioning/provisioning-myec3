<?php

namespace App\Product\Pastell;

use App\Entity\ProductDepartmentSocle;
use App\Exception\ProductOrganizationSocleNotFoundException;
use App\Repository\ProductDepartmentSocleRepository;
use App\Repository\ProductOrganizationSocleRepository;
use App\DepartmentSocleInterface;
use App\Service\PastellConnectorsService;
use PastellClient\Api\EntitesRequester;
use PastellClient\Hydrator\EntiteHydrator;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class DepartmentProvisioning implements DepartmentSocleInterface
{
    /**
     * @var ProductDepartmentSocleRepository
     */
    private $productDepartmentSocleRepository;
    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;
    /**
     * @var EntitesRequester
     */
    private $entitesRequester;
    /**
     * @var EntiteHydrator
     */
    private $entiteHydrator;
    /**
     * @var string
     */
    private $product;
    /**
     * @var int
     */
    private $number;
    /**
     * @var PastellConnectorsService
     */
    private $pastellConnectorsService;


    public function __construct(
        ProductDepartmentSocleRepository $productDepartmentSocleRepository,
        ProductOrganizationSocleRepository $productOrganizationSocleRepository,
        EntitesRequester $entitesRequester,
        PastellConnectorsService $pastellConnectorsService,
        string $product,
        int $number,
        EntiteHydrator $entiteHydrator = null
    ) {
        $this->productDepartmentSocleRepository = $productDepartmentSocleRepository;
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
        $this->entitesRequester = $entitesRequester;
        $this->pastellConnectorsService = $pastellConnectorsService;
        $this->product = $product;
        $this->number = $number;
        $this->entiteHydrator = $entiteHydrator ?? new EntiteHydrator();
    }

    public function getId(string $externalId)
    {
        if ($this->productDepartmentSocleRepository->exists($this->product, $this->number, $externalId)) {
            $productDepartmentSocle = $this->productDepartmentSocleRepository->get(
                $this->product,
                $this->number,
                $externalId
            );
            return $productDepartmentSocle->getDepartmentProductId();
        }
        return null;
    }

    /**
     * @throws ProductOrganizationSocleNotFoundException
     * @throws ClientExceptionInterface
     */
    public function add(SimpleXMLElement $xml): bool
    {
        $productDepartmentSocle = new ProductDepartmentSocle();
        $productDepartmentSocle->setProductId($this->product);
        $productDepartmentSocle->setProductNumber($this->number);
        $productDepartmentSocle->setDepartmentSocleId((string)$xml->externalId);

        if ((string)($xml->rootDepartment) === 'false') {
            $entite = $this->getEntiteFromXml($xml);
            $response = $this->entitesRequester->create($this->entiteHydrator->hydrate($entite));
            $entityId =  $response->getId();
            $productDepartmentSocle->setDepartmentProductId($entityId);
        } else {
            // Case when the department is the organism, the pastell entity already exist
            $productDepartmentSocle->setDepartmentProductId(
                $this->productOrganizationSocleRepository->get(
                    $this->product,
                    $this->number,
                    (string)$xml->organism->externalId
                )->getOrganizationProductId()
            );
            $entityId = $productDepartmentSocle->getDepartmentProductId();
        }

        $connector = $this->pastellConnectorsService->saveLibersignConnector($entityId);
        $this->pastellConnectorsService->associateLibersignConnector($connector);
        $this->pastellConnectorsService->saveTdTConnector($entityId, (string)$xml->organism->id);

        return $this->productDepartmentSocleRepository->add($productDepartmentSocle);
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function update(SimpleXMLElement $xml, $departmentId): bool
    {
        $entite = $this->getEntiteFromXml($xml);
        $pastellEntite = $this->entitesRequester->show($departmentId);
        $pastellEntite->denomination = $entite['denomination'];
        $pastellEntite->siren = $entite['siren'];
        $this->entitesRequester->update($pastellEntite);
        $this->pastellConnectorsService->saveLibersignConnector($pastellEntite->getId());
        $this->pastellConnectorsService->saveTdTConnector($pastellEntite->getId(), (string)$xml->organism->id);

        return true;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function delete($departmentId): bool
    {
        $productDepartmentSocle = $this->productDepartmentSocleRepository->getByDepartmentProductId(
            $this->product,
            $this->number,
            $departmentId
        );

        $this->entitesRequester->remove($departmentId);
        return $this->productDepartmentSocleRepository->delete($productDepartmentSocle);
    }

    private function getEntiteFromXml(SimpleXMLElement $xml): array
    {
        return [
            'socleId' => (string)$xml->externalId,
            'denomination' => (string)$xml->label,
            'siren' => (string)$xml->siren,
            'type' => 'collectivite',
            'entite_mere' => $this->productDepartmentSocleRepository
                ->get($this->product, $this->number, (string)$xml->parentDepartment->externalId)
                ->getDepartmentProductId(),
        ];
    }
}
