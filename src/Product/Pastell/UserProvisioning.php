<?php

namespace App\Product\Pastell;

use App\Entity\ProductUserSocle;
use App\Exception\ProductUserSocleNotFoundException;
use App\Repository\ProductUserSocleRepository;
use Exception;
use App\UserSocleInterface;
use PastellClient\Api\UserRolesRequester;
use PastellClient\Api\UsersRequester;
use PastellClient\Exception\PastellException;
use PastellClient\Hydrator\UserHydrator;
use PastellClient\Model\UserRole;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class UserProvisioning implements UserSocleInterface
{
    /**
     * @var ProductUserSocleRepository
     */
    private $productUserSocleRepository;

    /**
     * @var UsersRequester
     */
    private $usersRequester;

    /**
     * @var UserHydrator
     */
    private $userHydrator;

    /**
     * @var UserRolesRequester
     */
    private $userRolesRequester;
    /**
     * @var string
     */
    private $product;
    /**
     * @var int
     */
    private $number;

    public function __construct(
        ProductUserSocleRepository $productUserSocleRepository,
        UserRolesRequester $userRolesRequester,
        UsersRequester $usersRequester,
        string $product,
        int $number,
        UserHydrator $userHydrator = null
    ) {
        $this->productUserSocleRepository = $productUserSocleRepository;
        $this->userRolesRequester = $userRolesRequester;
        $this->usersRequester = $usersRequester;
        $this->product = $product;
        $this->number = $number;
        $this->userHydrator = $userHydrator ?? new UserHydrator();
    }

    /**
     * add user
     * @param SimpleXMLElement $xml
     * @param string|int $structureId
     * @param string|int $roleId
     * @return bool
     * @throws ClientExceptionInterface
     * @throws PastellException
     * @throws Exception
     */
    public function add(SimpleXMLElement $xml, $structureId, $roleId): bool
    {
        $user = $this->getUserFromXml($xml);
        $user['password'] = base64_encode(random_bytes(20));
        $user['id_e'] = $structureId;

        $userId = null;
        try {
            $response = $this->usersRequester->create($this->userHydrator->hydrate($user));
            $userId = $response->getId();
        } catch (PastellException $exception) {
            $message = json_decode($exception->getMessage(), true);
            if (!empty($message['error-message']) && $message['error-message'] === 'Ce login existe déjà') {
                $users = $this->usersRequester->all(['id_e' => $structureId]);
                foreach ($users as $currentUser) {
                    if ($currentUser->login === $user['login']) {
                        $userId = $currentUser->getId();
                    }
                }
            }
            if (is_null($userId)) {
                throw $exception;
            }
        }

        $userRole = new UserRole();
        $userRole->role = $roleId;
        $userRole->id_u = $userId;
        $userRole->id_e = $structureId;
        $this->userRolesRequester->add($userRole);

        $productUserSocle = new ProductUserSocle();
        $productUserSocle->setProductId($this->product);
        $productUserSocle->setProductNumber($this->number);
        $productUserSocle->setUserSocleId($user['socleId']);
        $productUserSocle->setUserProductId($userId);
        return $this->productUserSocleRepository->add($productUserSocle);
    }

    /**
     * update user
     * @param SimpleXMLElement $xml
     * @param string|int $userId
     * @param string|int $structureId
     * @param string|int $roleId
     * @param string|null $departmentId
     * @return bool
     * @throws ClientExceptionInterface
     */
    public function update(SimpleXMLElement $xml, $userId, $structureId, $roleId, ?string $departmentId = null): bool
    {
        $user = $this->getUserFromXml($xml);
        $pastellUser = $this->usersRequester->show($userId);
        $entityId = $pastellUser->id_e;

        if ($pastellUser->id_e !== (int)$departmentId) {
            $userRoles = $this->userRolesRequester->all($userId);

            $userEntityRoles = array_filter($userRoles, static function (UserRole $userRole) use ($entityId) {
                return $userRole->id_e === $entityId;
            });

            foreach ($userEntityRoles as $userRole) {
                $userRole->id_e = $departmentId;
                $this->userRolesRequester->add($userRole);
                $userRole->id_e = $entityId;
                $this->userRolesRequester->remove($userRole);
            }
        }

        $pastellUser->prenom = $user['prenom'];
        $pastellUser->nom = $user['nom'];
        $pastellUser->email = $user['email'];
        $pastellUser->id_e = $departmentId;
        $this->usersRequester->update($pastellUser);

        return true;
    }

    /**
     * delete user
     * @param string|int $userId user
     * @return bool
     * @throws ClientExceptionInterface
     */
    public function delete($userId): bool
    {
        $productUserSocle = $this->productUserSocleRepository->getByUserProductId(
            $this->product,
            $this->number,
            $userId
        );
        $this->usersRequester->remove($userId);

        return $this->productUserSocleRepository->delete($productUserSocle);
    }

    /**
     * get user by externalId
     * @param string $externalId
     * @return string|int
     * @throws ProductUserSocleNotFoundException
     */
    public function getId(string $externalId)
    {
        if (!$this->productUserSocleRepository->exists($this->product, $this->number, $externalId)) {
            return null;
        }
        return $this->productUserSocleRepository->get($this->product, $this->number, $externalId)->getUserProductId();
    }

    private function getUserFromXml(SimpleXMLElement $xml): array
    {
        return [
            'socleId' => (string)$xml->externalId,
            'login' => (string)$xml->user->username,
            'prenom' => (string)$xml->user->firstname,
            'nom' => (string)$xml->user->lastname,
            'email' => (string)$xml->email,
        ];
    }
}
