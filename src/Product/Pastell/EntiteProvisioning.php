<?php

namespace App\Product\Pastell;

use App\Entity\ProductOrganizationSocle;
use App\Exception\ProductOrganizationSocleNotFoundException;
use App\Repository\ProductOrganizationSocleRepository;
use App\Service\PastellTechnicalUserService;
use App\StructureSocleInterface;
use PastellClient\Api\EntitesRequester;
use PastellClient\Exception\PastellException;
use PastellClient\Hydrator\EntiteHydrator;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class EntiteProvisioning implements StructureSocleInterface
{
    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;

    /**
     * @var EntitesRequester
     */
    private $entitesRequester;

    /**
     * @var PastellTechnicalUserService
     */
    private $pastellTechnicalUserService;

    /**
     * @var EntiteHydrator
     */
    private $entiteHydrator;
    /**
     * @var string
     */
    private $product;
    /**
     * @var int
     */
    private $number;

    public function __construct(
        ProductOrganizationSocleRepository $productOrganizationSocleRepository,
        EntitesRequester $entitesRequester,
        PastellTechnicalUserService $pastellTechnicalUserService,
        string $product,
        int $number,
        EntiteHydrator $entiteHydrator = null
    ) {
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
        $this->entitesRequester = $entitesRequester;
        $this->pastellTechnicalUserService = $pastellTechnicalUserService;
        $this->product = $product;
        $this->number = $number;
        $this->entiteHydrator = $entiteHydrator ?? new EntiteHydrator();
    }

    /**
     * Add structure
     * @param SimpleXMLElement $xml
     * @return bool
     * @throws ClientExceptionInterface
     * @throws PastellException
     */
    public function add(SimpleXMLElement $xml): bool
    {
        $entite = $this->getEntiteFromXml($xml);

        $response = $this->entitesRequester->create($this->entiteHydrator->hydrate($entite));
        $this->pastellTechnicalUserService->create($xml, $response->getId());

        $productUserSocle = new ProductOrganizationSocle();
        $productUserSocle->setProductId($this->product);
        $productUserSocle->setProductNumber($this->number);
        $productUserSocle->setOrganizationSocleId($entite['socleId']);
        $productUserSocle->setOrganizationProductId($response->getId());
        return $this->productOrganizationSocleRepository->add($productUserSocle);
    }

    /**
     * update a structure
     * @param SimpleXMLElement $xml
     * @param string|int $structureId
     * @return bool
     * @throws ClientExceptionInterface
     */
    public function update(SimpleXMLElement $xml, $structureId): bool
    {
        $entite = $this->getEntiteFromXml($xml);
        $pastellEntite = $this->entitesRequester->show($structureId);
        $pastellEntite->denomination = $entite['denomination'];
        $pastellEntite->siren = $entite['siren'];
        $this->entitesRequester->update($pastellEntite);

        return true;
    }

    /**
     * @param string|int $structureId
     * @return bool
     * @throws ClientExceptionInterface
     */
    public function delete($structureId): bool
    {
        $productOrganizationSocle = $this->productOrganizationSocleRepository->getByOrganizationProductId(
            $this->product,
            $this->number,
            $structureId
        );

        $this->entitesRequester->remove($structureId);
        return $this->productOrganizationSocleRepository->delete($productOrganizationSocle);
    }

    /**
     * get structureId
     * @param string $externalId
     * @return string|int|null
     * @throws ProductOrganizationSocleNotFoundException
     */
    public function getId(string $externalId)
    {
        if ($this->productOrganizationSocleRepository->exists($this->product, $this->number, $externalId)) {
            $productOrganizationSocle = $this->productOrganizationSocleRepository->get($this->product, $this->number, $externalId);
            return $productOrganizationSocle->getOrganizationProductId();
        }
        return null;
    }

    /**
     * get role Id
     * @param string|int $roleName
     * @param string|int $structureId
     * @return string|int|null
     */
    public function getRoleId($roleName, $structureId)
    {
        return $roleName;
    }

    private function getEntiteFromXml(SimpleXMLElement $xml): array
    {
        return [
            'socleId' => (string)$xml->externalId,
            'denomination' => (string)$xml->label,
            'siren' => (string)$xml->siren,
            'type' => 'collectivite',
            'entite_mere' => '0'
        ];
    }
}
