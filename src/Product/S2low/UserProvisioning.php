<?php

namespace App\Product\S2low;

use App\Entity\ProductUserSocle;
use App\Exception\ProductUserSocleNotFoundException;
use App\Http\Client\S2lowClient;
use App\Http\Client\S2lowClientException;
use App\Http\Model\S2lowUser;
use App\Repository\ProductUserSocleRepository;
use App\UserSocleInterface;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class UserProvisioning implements UserSocleInterface
{
    /**
     * @var ProductUserSocleRepository
     */
    private $productUserSocleRepository;
    /**
     * @var S2lowClient
     */
    private $s2lowClient;
    /**
     * @var array
     */
    private $defaultUserValues;
    /**
     * @var string
     */
    private $product;
    /**
     * @var int
     */
    private $number;

    public function __construct(
        ProductUserSocleRepository $productUserSocleRepository,
        S2lowClient $s2lowClient,
        string $product,
        int $number,
        array $defaultUserValues
    ) {
        $this->productUserSocleRepository = $productUserSocleRepository;
        $this->s2lowClient = $s2lowClient;
        $this->product = $product;
        $this->number = $number;
        $this->defaultUserValues = $defaultUserValues;
    }

    /**
     * @inheritDoc
     * @throws S2lowClientException
     * @throws ClientExceptionInterface
     * @throws \Exception
     */
    public function add(SimpleXMLElement $xml, $structureId, $roleId): bool
    {
        $user = $this->getUserFromXml($xml);
        $user->authority_id = (int)$structureId;
        $user->authority_group_id = $this->defaultUserValues['authority_group_id'];
        $user->role = $this->defaultUserValues['role'];

        $user->setPassword(random_bytes(20));
        $user->setActPermission('RW');
        $user->setHeliosPermission('RW');

        $response = $this->s2lowClient->createOrUpdateUser($user);
        $userId = $response['id'];
        $services = $this->s2lowClient->getAuthorityServices($structureId);
        if (!empty($services)) {
            $this->s2lowClient->addUserToService($userId, $services[0]['id']);
        }

        $productUserSocle = new ProductUserSocle();
        $productUserSocle->setProductId($this->product);
        $productUserSocle->setProductNumber($this->number);
        $productUserSocle->setUserSocleId((string)$xml->externalId);
        $productUserSocle->setUserProductId($userId);
        return $this->productUserSocleRepository->add($productUserSocle);
    }

    /**
     * @inheritDoc
     * @throws S2lowClientException
     * @throws ClientExceptionInterface
     * @throws \Exception
     */
    public function update(SimpleXMLElement $xml, $userId, $structureId, $roleId, ?string $departmentId = null): bool
    {
        $user = $this->s2lowClient->getUser($userId);
        $user = $this->getUserFromXml($xml,$user);

        $this->s2lowClient->createOrUpdateUser($user);

        // $roleId contains the id of the organism so we don't have to heavily modify the Socle class
        $services = $this->s2lowClient->getAuthorityServices($roleId);
        if (!empty($services)) {
            $this->s2lowClient->addUserToService($userId, $services[0]['id']);
        }

        return true;
    }

    /**
     * @inheritDoc
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function delete($userId): bool
    {
        $productUserSocle = $this->productUserSocleRepository->getByUserProductId(
            $this->product,
            $this->number,
            $userId
        );
        $user = $this->s2lowClient->getUser($userId);
        $user->status = 0;
        $this->s2lowClient->createOrUpdateUser($user);

        return $this->productUserSocleRepository->delete($productUserSocle);
    }

    /**
     * @inheritDoc
     * @throws ProductUserSocleNotFoundException
     */
    public function getId(string $externalId)
    {
        if (!$this->productUserSocleRepository->exists($this->product, $this->number, $externalId)) {
            return null;
        }
        return $this->productUserSocleRepository->get($this->product, $this->number, $externalId)->getUserProductId();
    }

    /**
     * @throws \Exception
     */
    private function getUserFromXml(SimpleXMLElement $xml, ?S2lowUser $s2lowUser = null): S2lowUser
    {
        $user = $s2lowUser ?? new S2lowUser();
        $user->name = (string)$xml->user->lastname;
        $user->givenname = (string)$xml->user->firstname;
        $user->email = (string)$xml->email;
        $user->certificate = (string)$xml->user->certificate;
        $user->status = 1;
        $user->auth_method = 2;
        $user->login = 'socle_' . $xml->id;

        return $user;
    }
}
