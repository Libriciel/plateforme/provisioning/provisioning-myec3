<?php

namespace App\Product\S2low;

use App\Entity\ProductOrganizationSocle;
use App\Exception\ProductOrganizationSocleNotFoundException;
use App\Http\Client\S2lowClient;
use App\Http\Client\S2lowClientException;
use App\Http\Model\S2lowAuthority;
use App\Repository\ProductOrganizationSocleRepository;
use App\Service\S2lowTechnicalUserService;
use App\StructureSocleInterface;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class EntiteProvisioning implements StructureSocleInterface
{
    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;
    /**
     * @var S2lowClient
     */
    private $s2lowClient;
    /**
     * @var S2lowTechnicalUserService
     */
    private $s2lowTechnicalUserService;
    /**
     * @var array
     */
    private $defaultAuthorityValues;
    /**
     * @var string
     */
    private $product;
    /**
     * @var int
     */
    private $number;

    public function __construct(
        ProductOrganizationSocleRepository $productOrganizationSocleRepository,
        S2lowClient $s2lowClient,
        S2lowTechnicalUserService $s2lowTechnicalUserService,
        string $product,
        int $number,
        array $defaultAuthorityValues
    ) {
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
        $this->s2lowClient = $s2lowClient;
        $this->s2lowTechnicalUserService = $s2lowTechnicalUserService;
        $this->product = $product;
        $this->number = $number;
        $this->defaultAuthorityValues = $defaultAuthorityValues;
    }

    /**
     * @inheritDoc
     * @throws S2lowClientException
     * @throws ClientExceptionInterface
     */
    public function add(SimpleXMLElement $xml): bool
    {
        $authority = $this->getAuthorityFromXml($xml);
        $authority->authority_group_id = $this->defaultAuthorityValues['authority_group_id'];
        $authority->department = $this->defaultAuthorityValues['department'];
        $authority->district = $this->defaultAuthorityValues['district'];
        $authority->authority_type_id = $this->defaultAuthorityValues['authority_type_id'];
        $authority->setActModule();
        $authority->setHeliosModule();

        $response = $this->s2lowClient->createOrUpdateAuthority($authority);
        $this->s2lowClient->createAuthorityService($response['id'], $this->defaultAuthorityValues['service_name']);

        $this->s2lowClient->addSirenToGroup($authority->authority_group_id , $authority->siren);
        $this->s2lowTechnicalUserService->create((string)$xml->id, $response['id']);

        $productUserSocle = new ProductOrganizationSocle();
        $productUserSocle
            ->setProductId($this->product)
            ->setProductNumber($this->number)
            ->setOrganizationSocleId((string)$xml->externalId)
            ->setOrganizationProductId($response['id']);
        return $this->productOrganizationSocleRepository->add($productUserSocle);
    }

    /**
     * @inheritDoc
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function update(SimpleXMLElement $xml, $structureId): bool
    {
        $authority = $this->s2lowClient->getAuthority($structureId);
        $authority->name = (string)$xml->label;
        $authority->email = (string)$xml->email;
        $authority->address = (string)$xml->address->postalAddress;
        $authority->postal_code = (string)$xml->address->postalCode;
        $authority->city = (string)$xml->address->city;

        $response = $this->s2lowClient->createOrUpdateAuthority($authority);
        $services = $this->s2lowClient->getAuthorityServices($response['id']);
        if (empty($services)) {
            $this->s2lowClient->createAuthorityService($response['id'], $this->defaultAuthorityValues['service_name']);
        }

        return true;
    }

    /**
     * @inheritDoc
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function delete($structureId): bool
    {
        $productOrganizationSocle = $this->productOrganizationSocleRepository->getByOrganizationProductId(
            $this->product,
            $this->number,
            $structureId
        );
        $authority = $this->s2lowClient->getAuthority($structureId);
        $authority->status = 0;
        $this->s2lowClient->createOrUpdateAuthority($authority);

        return $this->productOrganizationSocleRepository->delete($productOrganizationSocle);
    }

    /**
     * @inheritDoc
     * @throws ProductOrganizationSocleNotFoundException
     */
    public function getId(string $externalId)
    {
        if ($this->productOrganizationSocleRepository->exists($this->product, $this->number, $externalId)) {
            $productOrganizationSocle = $this->productOrganizationSocleRepository->get(
                $this->product,
                $this->number,
                $externalId
            );
            return $productOrganizationSocle->getOrganizationProductId();
        }
        return null;
    }

    /**
     * @inheritDoc
     *
     * Returns the id of the organism so we don't have to heavily modify the Socle class
     */
    public function getRoleId($roleName, $structureId)
    {
        return $structureId;
    }

    private function getAuthorityFromXml(SimpleXMLElement $xml): S2lowAuthority
    {
        $authority = new S2lowAuthority();
        $authority->name = (string)$xml->label;
        $authority->siren = (string)$xml->siren;
        $authority->email = (string)$xml->email;
        $authority->status = 1;
        $authority->authority_type_id = (string)$xml->user->certificate;
        $authority->address = (string)$xml->address->postalAddress;
        $authority->postal_code = (string)$xml->address->postalCode;
        $authority->city = (string)$xml->address->city;

        return $authority;
    }
}
