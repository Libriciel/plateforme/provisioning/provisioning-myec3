<?php

namespace App\Product\ParapheurMultitenant;

use App\Entity\ProductOrganizationSocle;
use App\Http\Client\ParapheurClient;
use App\Http\Client\ParapheurClientException;
use App\Repository\ProductOrganizationSocleRepository;
use App\StructureSocleInterface;
use Doctrine\ORM\NonUniqueResultException;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class EntiteProvisioning implements StructureSocleInterface
{
    public const THRESHOLD_TENANT_NUMBER = 99;

    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;
    /**
     * @var ParapheurClient
     */
    private $parapheurClient;
    /**
     * @var string
     */
    private $product;
    /**
     * @var int
     */
    private $number;
    /**
     * @var string
     */
    private $adminPassword;

    public function __construct(
        ProductOrganizationSocleRepository $productOrganizationSocleRepository,
        ParapheurClient $parapheurClient,
        string $product,
        int $number,
        string $adminPassword
    ) {
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
        $this->parapheurClient = $parapheurClient;
        $this->product = $product;
        $this->number = $number;
        $this->adminPassword = $adminPassword;
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function add(SimpleXMLElement $xml): bool
    {
        $tenantName = $xml->tenantIdentifier;
        if ($this->parapheurClient->tenantExists($tenantName)) {
            $this->parapheurClient->enableTenant($tenantName);
        } else {
            $tenants = $this->parapheurClient->getTenants();
            $tenantsNumber = count($tenants);
            if ($tenantsNumber >= self::THRESHOLD_TENANT_NUMBER) {
                throw new \OverflowException("The number of tenants cannot go beyond the threshold value of : " . self::THRESHOLD_TENANT_NUMBER);
            }
            $this->parapheurClient->createTenant($tenantName, $tenantName, $tenantName, $this->adminPassword);
        }

        $productOrganizationSocle = new ProductOrganizationSocle();
        $productOrganizationSocle->setProductId($this->product);
        $productOrganizationSocle->setProductNumber($this->number);
        $productOrganizationSocle->setOrganizationSocleId($xml->externalId);
        $productOrganizationSocle->setOrganizationProductId($tenantName);
        return $this->productOrganizationSocleRepository->add($productOrganizationSocle);
    }

    public function update(SimpleXMLElement $xml, $structureId): bool
    {
        return true;
    }

    /**
     * @throws NonUniqueResultException
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function delete($structureId): bool
    {
        $productOrganizationSocle = $this->productOrganizationSocleRepository->getByOrganizationProductId(
            $this->product,
            $this->number,
            $structureId
        );
        $this->parapheurClient->disableTenant($structureId);

        return $this->productOrganizationSocleRepository->delete($productOrganizationSocle);
    }

    /**
     * @throws NonUniqueResultException
     */
    public function getId(string $externalId)
    {
        if ($this->productOrganizationSocleRepository->exists($this->product, $this->number, $externalId)) {
            $productOrganizationSocle = $this->productOrganizationSocleRepository->get(
                $this->product,
                $this->number,
                $externalId
            );
            return $productOrganizationSocle->getOrganizationProductId();
        }

        return null;
    }

    public function getRoleId($roleName, $structureId)
    {
        return $roleName;
    }
}
