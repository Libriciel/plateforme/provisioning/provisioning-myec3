<?php

namespace App\Product\Parapheur;

use App\Entity\ProductOrganizationSocle;
use App\Http\Client\ParapheurClientException;
use App\Repository\ProductOrganizationSocleRepository;
use App\StructureSocleInterface;
use App\Http\Client\ParapheurClient;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class EntiteProvisioning implements StructureSocleInterface
{

    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;

    /**
     * @var ParapheurClient
     */
    private $parapheurClient;
    /**
     * @var string
     */
    private $product;
    /**
     * @var int
     */
    private $number;


    public function __construct(
        ProductOrganizationSocleRepository $productOrganizationSocleRepository,
        ParapheurClient $parapheurClient,
        string $product,
        int $number
    ) {
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
        $this->parapheurClient = $parapheurClient;
        $this->product = $product;
        $this->number = $number;
    }

    /**
     * @inheritDoc
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function add(SimpleXMLElement $xml): bool
    {
        $socleId = $xml->id;
        $groupId = $this->parapheurClient->getGroupByName($socleId);
        if ($groupId === null) {
            $response = $this->parapheurClient->createGroup($socleId);
            $groupId = $response['id'];
        }
        $productOrganizationSocle = new ProductOrganizationSocle();
        $productOrganizationSocle->setProductId($this->product);
        $productOrganizationSocle->setProductNumber($this->number);
        $productOrganizationSocle->setOrganizationSocleId($socleId);
        $productOrganizationSocle->setOrganizationProductId($groupId);
        return $this->productOrganizationSocleRepository->add($productOrganizationSocle);
    }

    /**
     * @inheritDoc
     */
    public function update(SimpleXMLElement $xml, $structureId): bool
    {
        // There is nothing to update in a group
        return true;
    }

    /**
     * @inheritDoc
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function delete($structureId): bool
    {
        $productOrganizationSocle = $this->productOrganizationSocleRepository->getByOrganizationProductId(
            $this->product,
            $this->number,
            $structureId
        );
        $this->parapheurClient->deleteGroup($structureId);

        return $this->productOrganizationSocleRepository->delete($productOrganizationSocle);
    }

    /**
     * @inheritDoc
     */
    public function getId(string $externalId)
    {
        if ($this->productOrganizationSocleRepository->exists($this->product, $this->number, $externalId)) {
            $productOrganizationSocle = $this->productOrganizationSocleRepository->get(
                $this->product,
                $this->number,
                $externalId
            );
            return $productOrganizationSocle->getOrganizationProductId();
        }

        return $this->parapheurClient->getGroupByName($externalId);
    }

    /**
     * @inheritDoc
     */
    public function getRoleId($roleName, $structureId)
    {
        return $roleName;
    }
}
