<?php

namespace App\Product\Parapheur;

use App\Entity\ProductUserSocle;
use App\Repository\ProductUserSocleRepository;
use Exception;
use App\UserSocleInterface;
use App\Exception\ProductUserSocleNotFoundException;
use App\Http\Client\ParapheurClient;
use App\Http\Client\ParapheurClientException;
use Psr\Http\Client\ClientExceptionInterface;
use SimpleXMLElement;

class UserProvisioning implements UserSocleInterface
{
    /**
     * @var ProductUserSocleRepository
     */
    private $productUserSocleRepository;

    /**
     * @var ParapheurClient
     */
    private $parapheurClient;
    /**
     * @var string
     */
    private $product;
    /**
     * @var int
     */
    private $number;


    public function __construct(
        ProductUserSocleRepository $productUserSocleRepository,
        ParapheurClient $parapheurClient,
        string $product,
        int $number
    ) {
        $this->productUserSocleRepository = $productUserSocleRepository;
        $this->parapheurClient = $parapheurClient;
        $this->product = $product;
        $this->number = $number;
    }

    /**
     * @inheritDoc
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     * @throws Exception
     */
    public function add(SimpleXMLElement $xml, $structureId, $roleId): bool
    {
        $user = $this->getUserFromXml($xml);

        $response = $this->parapheurClient->createUser(
            $user['login'],
            $user['prenom'],
            $user['nom'],
            $user['email'],
            base64_encode(random_bytes(20))
        );

        $this->parapheurClient->addUserToGroup($structureId, $user['login']);

        $productUserSocle = new ProductUserSocle();
        $productUserSocle->setProductId($this->product);
        $productUserSocle->setProductNumber($this->number);
        $productUserSocle->setUserSocleId($user['socleId']);
        $productUserSocle->setUserProductId($response['id']);
        return $this->productUserSocleRepository->add($productUserSocle);
    }

    /**
     * @inheritDoc
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function update(SimpleXMLElement $xml, $userId, $structureId, $roleId, ?string $departmentId = null): bool
    {
        $user = $this->getUserFromXml($xml);
        $this->parapheurClient->updateUser($userId, $user['prenom'], $user['nom'], $user['email']);

        return true;
    }

    /**
     * @inheritDoc
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function delete($userId): bool
    {
        $productUserSocle = $this->productUserSocleRepository->getByUserProductId(
            $this->product,
            $this->number,
            $userId
        );
        $this->parapheurClient->deleteUser($userId);

        return $this->productUserSocleRepository->delete($productUserSocle);
    }

    /**
     * @inheritDoc
     * @throws ProductUserSocleNotFoundException
     */
    public function getId(string $externalId)
    {
        if (!$this->productUserSocleRepository->exists(
            $this->product,
            $this->number,
            $externalId
        )) {
            return null;
        }
        return $this->productUserSocleRepository->get($this->product, $this->number, $externalId)->getUserProductId();
    }

    private function getUserFromXml(SimpleXMLElement $xml)
    {
        return [
            'socleId' => (string)$xml->externalId,
            'login' => (string)$xml->alfUserName,
            'prenom' => (string)$xml->user->firstname,
            'nom' => (string)$xml->user->lastname,
            'email' => (string)$xml->email,
        ];
    }
}
