<?php
/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 04/11/19
 * Time: 17:05
 */

namespace App;


use SimpleXMLElement;

interface UserSocleInterface
{
    /**
     * add user
     * @param SimpleXMLElement $xml
     * @param string|int $structureId
     * @param string|int $roleId
     * @return bool
     */
    public function add(SimpleXMLElement $xml, $structureId, $roleId): bool;

    /**
     * update user
     * @param SimpleXMLElement $xml
     * @param string|int $userId
     * @param string|int $structureId
     * @param string|int $roleId
     */
    public function update(SimpleXMLElement $xml, $userId, $structureId, $roleId, ?string $departmentId = null): bool;

    /**
     * delete user
     * @param string|int $userId user
     * @return bool
     */
    public function delete($userId): bool;

    /**
     * get user by externalId
     * @param string $externalId
     * @return string|int
     */
    public function getId(string $externalId);
}
