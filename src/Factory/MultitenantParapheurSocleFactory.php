<?php

namespace App\Factory;

use App\Http\Client\ParapheurClient;
use App\Product\ParapheurMultitenant\EntiteProvisioning;
use App\Product\ParapheurMultitenant\UserProvisioning;
use App\Repository\ProductOrganizationSocleRepository;
use App\Repository\ProductUserSocleRepository;
use App\Socle;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Psr18Client;

class MultitenantParapheurSocleFactory implements SocleFactoryInterface
{
    /**
     * @var ProductUserSocleRepository
     */
    private $productUserSocleRepository;
    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;

    public function __construct(
        ProductUserSocleRepository $productUserSocleRepository,
        ProductOrganizationSocleRepository $productOrganizationSocleRepository
    ) {
        $this->productUserSocleRepository = $productUserSocleRepository;
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
    }

    public function create(string $product, int $number): Socle
    {
        $url = getenv('PARAPHEUR_MULTI_URL_' . $number);
        $baseApi = getenv('PARAPHEUR_MULTI_BASE_API_' . $number);
        $username = getenv('PARAPHEUR_MULTI_USERNAME_' . $number);
        $password = getenv('PARAPHEUR_MULTI_PASSWORD_' . $number);
        $verifyPeer = getenv('PARAPHEUR_MULTI_VERIFYPEER_' . $number);
        $verifyHost = getenv('PARAPHEUR_MULTI_VERIFYHOST_' . $number);
        $proxy = getenv('PARAPHEUR_MULTI_PROXY_' . $number);
        $tenantPasswordAdmin = getenv('PARAPHEUR_MULTI_PASSWORD_ADMIN_' . $number);

        $psr18Client = new Psr18Client(
            HttpClient::createForBaseUri(
                $url,
                [
                    'verify_peer' => $verifyPeer,
                    'verify_host' => $verifyHost,
                    'proxy' => $proxy,
                ]
            )
        );
        $httpClient = new ParapheurClient($psr18Client);
        $httpClient->setBaseApi($baseApi);
        $httpClient->setCredentials($username, $password);

        $userProvisioning = new UserProvisioning(
            $this->productUserSocleRepository,
            $httpClient,
            $product,
            $number,
            $tenantPasswordAdmin
        );
        $entiteProvisioning = new EntiteProvisioning(
            $this->productOrganizationSocleRepository,
            $httpClient,
            $product,
            $number,
            $tenantPasswordAdmin
        );

        return new Socle($userProvisioning, $entiteProvisioning);
    }
}
