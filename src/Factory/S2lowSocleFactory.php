<?php

namespace App\Factory;

use App\Http\Client\S2lowClient;
use App\Product\S2low\EntiteProvisioning;
use App\Product\S2low\UserProvisioning;
use App\Repository\ProductOrganizationSocleRepository;
use App\Repository\ProductUserSocleRepository;
use App\Service\HashService;
use App\Service\S2lowTechnicalUserService;
use App\Socle;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Psr18Client;

class S2lowSocleFactory implements SocleFactoryInterface
{

    /**
     * @var ProductUserSocleRepository
     */
    private $productUserSocleRepository;
    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;
    /**
     * @var HashService
     */
    private $hashService;

    public function __construct(
        ProductUserSocleRepository $productUserSocleRepository,
        ProductOrganizationSocleRepository $productOrganizationSocleRepository,
        HashService $hashService

    ) {
        $this->productUserSocleRepository = $productUserSocleRepository;
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
        $this->hashService = $hashService;
    }

    public function create(string $product, int $number): Socle
    {
        $httpClient = $this->getHttpClient($number);

        return new Socle(
            $this->getUserProvisioning($httpClient, $product, $number),
            $this->getEntiteProvisioning($httpClient, $product, $number)
        );
    }

    private function getHttpClient(int $number): S2lowClient
    {
        $url = getenv('S2LOW_URL_' . $number);
        $verifyPeer = getenv('S2LOW_VERIFYPEER_' . $number);
        $verifyHost = getenv('S2LOW_VERIFYHOST_' . $number);
        $proxy = getenv('S2LOW_PROXY_' . $number);
        $localCert = getenv('S2LOW_CERTIFICATE_PATH_' . $number);
        $localPk = getenv('S2LOW_KEY_PATH_' . $number);
        $passphrase = getenv('S2LOW_KEY_PASSWORD_' . $number);

        $psr18Client = new Psr18Client(
            HttpClient::createForBaseUri(
                $url,
                [
                    'verify_peer' => $verifyPeer,
                    'verify_host' => $verifyHost,
                    'proxy' => $proxy,
                    'local_cert' => $localCert,
                    'local_pk' => $localPk,
                    'passphrase' => $passphrase,
                    // Workaround for S2lowClient::addSirenToGroup which is not a real API endpoint
                    'max_redirects' => 0,
                ]
            )
        );
        return new S2lowClient($psr18Client);
    }

    private function getUserProvisioning(S2lowClient $client, string $product, int $number): UserProvisioning
    {
        return new UserProvisioning(
            $this->productUserSocleRepository,
            $client,
            $product,
            $number,
            [
                'authority_group_id' => getenv('S2LOW_AUTHORITY_GROUP_ID_' . $number),
                'role' => getenv('S2LOW_USER_ROLE_' . $number),
            ]
        );
    }

    private function getEntiteProvisioning(S2lowClient $client, string $product, int $number): EntiteProvisioning
    {
        return new EntiteProvisioning(
            $this->productOrganizationSocleRepository,
            $client,
            $this->getTechnicalUserCreationService($client, $number),
            $product,
            $number,
            [
                'authority_group_id' => getenv('S2LOW_AUTHORITY_GROUP_ID_' . $number),
                'authority_type_id' => getenv('S2LOW_AUTHORITY_TYPE_ID_' . $number),
                'department' => getenv('S2LOW_AUTHORITY_DEPARTMENT_' . $number),
                'district' => getenv('S2LOW_AUTHORITY_DISTRICT_' . $number),
                'service_name' => getenv('S2LOW_SERVICE_NAME_' . $number),
            ]
        );
    }

    private function getTechnicalUserCreationService(S2lowClient $client, int $number): S2lowTechnicalUserService
    {
        return new S2lowTechnicalUserService(
            $client,
            $this->hashService,
            [
                'email' => getenv('S2LOW_TECH_USER_MAIL_' . $number),
                'certificate' => file_get_contents(getenv('S2LOW_TECH_USER_CERT_PATH_' . $number)),
                'authority_group_id' => getenv('S2LOW_AUTHORITY_GROUP_ID_' . $number),
                'role' => getenv('S2LOW_TECH_USER_ROLE_' . $number),
            ]
        );
    }
}
