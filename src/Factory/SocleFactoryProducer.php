<?php

namespace App\Factory;

use App\Enum\ProductEnum;
use App\Exception\SocleFactoryProducerException;
use App\Socle;

class SocleFactoryProducer
{
    /**
     * @var PastellSocleFactory
     */
    private $pastellSocleFactory;
    /**
     * @var S2lowSocleFactory
     */
    private $s2lowSocleFactory;
    /**
     * @var MonotenantParapheurSocleFactory
     */
    private $monotenantParapheurSocleFactory;
    /**
     * @var MultitenantParapheurSocleFactory
     */
    private $multitenantParapheurSocleFactory;

    public function __construct(
        PastellSocleFactory $pastellSocleFactory,
        S2lowSocleFactory $s2lowSocleFactory,
        MonotenantParapheurSocleFactory $monotenantParapheurSocleFactory,
        MultitenantParapheurSocleFactory $multitenantParapheurSocleFactory
    ) {
        $this->pastellSocleFactory = $pastellSocleFactory;
        $this->s2lowSocleFactory = $s2lowSocleFactory;
        $this->monotenantParapheurSocleFactory = $monotenantParapheurSocleFactory;
        $this->multitenantParapheurSocleFactory = $multitenantParapheurSocleFactory;
    }

    /**
     * @throws SocleFactoryProducerException
     */
    public function create(string $product, int $number): Socle
    {
        if ($product === ProductEnum::PASTELL) {
            return $this->pastellSocleFactory->create($product, $number);
        }
        if ($product === ProductEnum::S2LOW) {
            return $this->s2lowSocleFactory->create($product, $number);
        }
        if ($product === ProductEnum::PARAPHEUR_MONOTENANT) {
            return $this->monotenantParapheurSocleFactory->create($product, $number);
        }
        if ($product === ProductEnum::PARAPHEUR_MULTITENANT) {
            return $this->multitenantParapheurSocleFactory->create($product, $number);
        }

        throw new SocleFactoryProducerException("Unable to find a factory for the product : $product");
    }
}
