<?php

namespace App\Factory;

use App\Http\Client\ParapheurClient;
use App\Product\Parapheur\EntiteProvisioning;
use App\Product\Parapheur\UserProvisioning;
use App\Repository\ProductOrganizationSocleRepository;
use App\Repository\ProductUserSocleRepository;
use App\Socle;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Psr18Client;

class MonotenantParapheurSocleFactory implements SocleFactoryInterface
{
    /**
     * @var ProductUserSocleRepository
     */
    private $productUserSocleRepository;
    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;

    public function __construct(
        ProductUserSocleRepository $productUserSocleRepository,
        ProductOrganizationSocleRepository $productOrganizationSocleRepository
    ) {
        $this->productUserSocleRepository = $productUserSocleRepository;
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
    }

    public function create(string $product, int $number): Socle
    {
        $url = getenv('PARAPHEUR_URL_' . $number);
        $baseApi = getenv('PARAPHEUR_BASE_API_' . $number);
        $username = getenv('PARAPHEUR_USERNAME_' . $number);
        $password = getenv('PARAPHEUR_PASSWORD_' . $number);
        $verifyPeer = getenv('PARAPHEUR_VERIFYPEER_' . $number);
        $verifyHost = getenv('PARAPHEUR_VERIFYHOST_' . $number);
        $proxy = getenv('PARAPHEUR_PROXY_' . $number);

        $psr18Client = new Psr18Client(
            HttpClient::createForBaseUri(
                $url,
                [
                    'verify_peer' => $verifyPeer,
                    'verify_host' => $verifyHost,
                    'proxy' => $proxy,
                ]
            )
        );
        $httpClient = new ParapheurClient($psr18Client);
        $httpClient->setBaseApi($baseApi);
        $httpClient->setCredentials($username, $password);

        $userProvisioning = new UserProvisioning(
            $this->productUserSocleRepository,
            $httpClient,
            $product,
            $number
        );
        $entiteProvisioning = new EntiteProvisioning(
            $this->productOrganizationSocleRepository,
            $httpClient,
            $product,
            $number
        );

        return new Socle($userProvisioning, $entiteProvisioning);
    }
}
