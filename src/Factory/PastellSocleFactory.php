<?php

namespace App\Factory;

use App\Product\Pastell\DepartmentProvisioning;
use App\Product\Pastell\EntiteProvisioning;
use App\Product\Pastell\UserProvisioning;
use App\Repository\ProductDepartmentSocleRepository;
use App\Repository\ProductOrganizationSocleRepository;
use App\Repository\ProductUserSocleRepository;
use App\Service\HashService;
use App\Service\PastellConnectorsService;
use App\Service\PastellTechnicalUserService;
use App\Socle;
use PastellClient\Api\ConnectorsRequester;
use PastellClient\Api\EntitesRequester;
use PastellClient\Api\ModuleAssociationsRequester;
use PastellClient\Api\UserRolesRequester;
use PastellClient\Api\UsersRequester;
use PastellClient\Client;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpClient\Psr18Client;

class PastellSocleFactory implements SocleFactoryInterface
{

    /**
     * @var ProductUserSocleRepository
     */
    private $productUserSocleRepository;
    /**
     * @var ProductOrganizationSocleRepository
     */
    private $productOrganizationSocleRepository;
    /**
     * @var ProductDepartmentSocleRepository
     */
    private $productDepartmentSocleRepository;
    /**
     * @var HashService
     */
    private $hashService;

    public function __construct(
        ProductUserSocleRepository $productUserSocleRepository,
        ProductOrganizationSocleRepository $productOrganizationSocleRepository,
        ProductDepartmentSocleRepository $productDepartmentSocleRepository,
        HashService $hashService
    ) {
        $this->productUserSocleRepository = $productUserSocleRepository;
        $this->productOrganizationSocleRepository = $productOrganizationSocleRepository;
        $this->productDepartmentSocleRepository = $productDepartmentSocleRepository;
        $this->hashService = $hashService;
    }

    public function create(string $product, int $number): Socle
    {
        $httpClient = $this->getHttpClient($number);
        return new Socle(
            $this->getUserProvisioning($httpClient, $product, $number),
            $this->getEntiteProvisioning($httpClient, $product, $number),
            $this->getDepartmentProvisioning($httpClient, $product, $number)
        );
    }

    private function getHttpClient(int $number): Client
    {
        $url = getenv('PASTELL_URL_' . $number);
        $username = getenv('PASTELL_USERNAME_' . $number);
        $password = getenv('PASTELL_PASSWORD_' . $number);
        $verifyPeer = getenv('PASTELL_VERIFYPEER_' . $number);
        $verifyHost = getenv('PASTELL_VERIFYHOST_' . $number);
        $proxy = getenv('PASTELL_PROXY_' . $number);

        $psr18Client = new Psr18Client(
            HttpClient::create(
                [
                    'verify_peer' => $verifyPeer,
                    'verify_host' => $verifyHost,
                    'proxy' => $proxy,
                ]
            )
        );
        $httpClient = Client::createWithHttpClient($psr18Client, $url);
        $httpClient->authenticate($username, $password);
        return $httpClient;
    }

    private function getUserProvisioning(Client $httpClient, string $product, int $number): UserProvisioning
    {
        return new UserProvisioning(
            $this->productUserSocleRepository,
            new UserRolesRequester($httpClient),
            new UsersRequester($httpClient),
            $product,
            $number
        );
    }

    private function getEntiteProvisioning(Client $httpClient, string $product, int $number): EntiteProvisioning
    {
        return new EntiteProvisioning(
            $this->productOrganizationSocleRepository,
            new EntitesRequester($httpClient),
            $this->getTechnicalUserCreationService($httpClient, $number),
            $product,
            $number
        );
    }

    private function getDepartmentProvisioning(Client $httpClient, string $product, int $number): DepartmentProvisioning
    {
        return new DepartmentProvisioning(
            $this->productDepartmentSocleRepository,
            $this->productOrganizationSocleRepository,
            new EntitesRequester($httpClient),
            $this->getConnectorsCreationService($httpClient,$number),
            $product,
            $number
        );
    }

    private function getTechnicalUserCreationService(Client $httpClient, int $number): PastellTechnicalUserService
    {
        $email = getenv('PASTELL_TECH_USER_MAIL_' . $number);
        $roles = getenv('PASTELL_TECH_USER_ROLES_' . $number);

        return new PastellTechnicalUserService(
            new UsersRequester($httpClient),
            new UserRolesRequester($httpClient),
            $this->hashService,
            $email,
            json_decode($roles, true)
        );
    }

    private function getConnectorsCreationService(Client $httpClient, int $number): PastellConnectorsService
    {
        $options = [
            PastellConnectorsService::LIBERSIGN => [
                'label' => getenv('PASTELL_LIBERSIGN_LABEL_' . $number),
                'libersign_applet_url' => getenv('PASTELL_LIBERSIGN_APPLET_URL_' . $number),
                'libersign_extension_update_url' => getenv('PASTELL_LIBERSIGN_EXT_UPDATE_URL_' . $number),
                'libersign_help_url' => getenv('PASTELL_LIBERSIGN_HELP_URL_' . $number),
                'libersign_xmlstarlet_path' => getenv('PASTELL_LIBERSIGN_XMLSTARLET_PATH_' . $number),
                'associations' => json_decode(getenv('PASTELL_LIBERSIGN_ASSOCIATIONS_' . $number), true),
            ],
            PastellConnectorsService::S2LOW => [
                'label' => getenv('PASTELL_S2LOW_LABEL_' . $number),
                'url' => getenv('PASTELL_S2LOW_URL_' . $number),
                'user_certificat' => [
                    'filepath' => getenv('PASTELL_S2LOW_USER_CERT_PATH_' . $number),
                    'password' => getenv('PASTELL_S2LOW_USER_CERT_PASSWORD_' . $number),
                ],
                'authentication_for_teletransmisson' => getenv('PASTELL_S2LOW_AUTH_FOR_TELETRANSMISSION_' . $number),
                'associations' => json_decode(getenv('PASTELL_S2LOW_ASSOCIATIONS_' . $number), true),
            ]
        ];

        return new PastellConnectorsService(
            new ConnectorsRequester($httpClient),
            new ModuleAssociationsRequester($httpClient),
            $this->hashService,
            $options
        );
    }
}
