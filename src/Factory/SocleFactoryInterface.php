<?php

namespace App\Factory;

use App\Socle;

interface SocleFactoryInterface
{
    public function create(string $product, int $number): Socle;
}
