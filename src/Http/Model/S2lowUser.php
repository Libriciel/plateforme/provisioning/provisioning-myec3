<?php

namespace App\Http\Model;

class S2lowUser
{

    /**
     * @var int $api
     */
    public $api = 1;
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var string $name
     */
    public $name;
    /**
     * @var string $givenname
     */
    public $givenname;
    /**
     * @var string $email
     */
    public $email;
    /**
     * @var int $authority_id
     */
    public $authority_id;
    /**
     * @var int $status
     */
    public $status;
    /**
     * @var string $role
     */
    public $role;
    /**
     * @var int $authority_group_id
     */
    public $authority_group_id;
    /**
     * @var string $certificate
     */
    public $certificate;
    /**
     * @var int $auth_method
     */
    public $auth_method;

    /** @var string $login */
    public $login;

    /** @var string $password */
    private $password;

    /** @var string $perm_1 */
    private $perm_1;

    /** @var string $perm_2 */
    private $perm_2;

    public function getMultipartArray(): array
    {
        $array = [];
        foreach (get_object_vars($this) as $publicProperty => $value) {
            if (isset($value)) {
                $currentProperty = [
                    'name' => $publicProperty,
                    'contents' => $value,
                ];
                if ($publicProperty === 'certificate') {
                    // Looks like the filename is needed to have it populated in $_FILES in s2low
                    $currentProperty['filename'] = 'certificate.pem';
                }

                $array[] = $currentProperty;
            }
        }
        return $array;
    }

    public static function hydrate(array $data, int $id): self
    {
        $user = new self();
        $user->setId($id);
        $user->name = $data['name'];
        $user->givenname = $data['givenname'];
        $user->email = $data['email'];
        $user->authority_id = $data['authority_id'];
        $user->status = $data['status'];
        $user->role = $data['role'];
        $user->authority_group_id = $data['authority_group_id'];
        $user->certificate = $data['certificate'];
        $user->login = $data['login'];

        if (isset($data['module'][1])) {
            $user->setActPermission($data['module'][1]);
        }
        if (isset($data['module'][2])) {
            $user->setHeliosPermission($data['module'][2]);
        }
        return $user;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getActPermission(): string
    {
        return $this->perm_1;
    }

    public function setActPermission(string $permission): void
    {
        $this->perm_1 = $permission;
    }

    public function getHeliosPermission(): string
    {
        return $this->perm_2;
    }

    public function setHeliosPermission(string $permission): void
    {
        $this->perm_2 = $permission;
    }

    public function setPassword(string $password): S2lowUser
    {
        $this->password = $password;
        return $this;
    }
}
