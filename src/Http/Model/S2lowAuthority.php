<?php

namespace App\Http\Model;

class S2lowAuthority
{
    /**
     * @var int $api
     */
    public $api = 1;
    /**
     * @var int $id
     */
    private $id;
    /**
     * @var string $name
     */
    public $name;
    /**
     * @var string $siren
     */
    public $siren;
    /**
     * @var int $authority_group_id
     */
    public $authority_group_id;
    /**
     * @var string $email
     */
    public $email;
    /**
     * @var int $status
     */
    public $status;
    /**
     * @var int $authority_type_id
     */
    public $authority_type_id;
    /**
     * @var string $address
     */
    public $address;
    /**
     * @var string $postal_code
     */
    public $postal_code;
    /**
     * @var string $city
     */
    public $city;
    /**
     * @var string $department
     */
    public $department;
    /**
     * @var int $district
     */
    public $district;

    /** @var string $helios_ftp_dest */
    public $helios_ftp_dest;

    /**
     * @var int $perm_1
     */
    private $perm_1;
    /**
     * @var int $perm_2
     */
    private $perm_2;
    /**
     * @var int $perm_4
     */
    private $perm_4;

    public static function hydrate(array $bodyFormatted): self
    {
        $authority = new self();
        $authority->setId($bodyFormatted['id']);
        $authority->name = $bodyFormatted['name'];
        $authority->siren = $bodyFormatted['siren'];
        $authority->authority_group_id = $bodyFormatted['authority_group_id'];
        $authority->email = $bodyFormatted['email'];
        $authority->status = $bodyFormatted['status'];
        $authority->authority_type_id = $bodyFormatted['authority_type_id'];
        $authority->address = $bodyFormatted['address'];
        $authority->postal_code = $bodyFormatted['postal_code'];
        $authority->city = $bodyFormatted['city'];
        $authority->department = $bodyFormatted['department'];
        $authority->district = $bodyFormatted['district'];
        $authority->helios_ftp_dest = $bodyFormatted['helios_ftp_dest'] ?? '';

        if ($bodyFormatted['module'][1]) {
            $authority->setActModule();
        }
        if ($bodyFormatted['module'][2]) {
            $authority->setHeliosModule();
        }
        if ($bodyFormatted['module'][4]) {
            $authority->setMailModule();
        }
        return $authority;
    }

    public function toArray(): array
    {
        $array = [];
        foreach (get_object_vars($this) as $publicProperty => $value) {
            $array[$publicProperty] = utf8_decode($value);
        }
        return $array;
    }

    public function setActModule(): void
    {
        $this->perm_1 = 1;
    }

    public function setHeliosModule(): void
    {
        $this->perm_2 = 1;
    }

    public function setMailModule(): void
    {
        $this->perm_4 = 1;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
