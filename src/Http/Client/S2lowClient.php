<?php

namespace App\Http\Client;

use GuzzleHttp\Psr7\MultipartStream;
use Http\Discovery\Psr17FactoryDiscovery;
use Http\Discovery\Psr18ClientDiscovery;
use App\Http\Model\S2lowAuthority;
use App\Http\Model\S2lowUser;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\StreamFactoryInterface;
use function http_build_query;
use function json_decode;

class S2lowClient
{
    public const TEST_AUTHENTICATION_ENDPOINT = '/api/test-connexion.php';
    public const CREATE_USER_ENDPOINT = '/admin/users/admin_user_edit_handler.php';
    public const GET_USER_ENDPOINT = '/admin/users/admin_user_detail.php';
    public const CREATE_AUTHORITY_ENDPOINT = '/admin/authorities/admin_authority_edit_handler.php';
    public const GET_AUTHORITY_ENDPOINT = '/admin/authorities/admin_authority_detail.php';
    public const GET_AUTHORITY_SERVICES_ENDPOINT = '/admin/services/list-service.php';
    public const CREATE_AUTHORITY_SERVICE_ENDPOINT = '/admin/services/add-service-user.php';
    public const ADD_USER_TO_SERVICE_ENDPOINT = '/admin/users/add-user-to-service.php';
    public const ADD_SIREN_TO_GROUP_ENDPOINT = '/admin/groups/add-siren-controler.php';

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @var RequestFactoryInterface
     */
    private $requestFactory;

    /**
     * @var StreamFactoryInterface
     */
    private $streamFactory;

    public function __construct(
        ClientInterface $httpClient = null,
        RequestFactoryInterface $requestFactory = null,
        StreamFactoryInterface $streamFactory = null
    ) {
        $this->httpClient = $httpClient ?? Psr18ClientDiscovery::find();
        $this->requestFactory = $requestFactory ?? Psr17FactoryDiscovery::findRequestFactory();
        $this->streamFactory = $streamFactory ?? Psr17FactoryDiscovery::findStreamFactory();
    }

    /**
     * @throws S2lowClientException
     * @throws ClientExceptionInterface
     */
    public function testAuth(): string
    {
        $request = $this->requestFactory->createRequest('GET', self::TEST_AUTHENTICATION_ENDPOINT);
        $response = $this->httpClient->sendRequest($request);

        $body = (string)$response->getBody();
        if ($response->getStatusCode() !== 200) {
            throw new S2lowClientException($body, $response->getStatusCode());
        }
        return $body;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function createOrUpdateUser(S2lowUser $user): array
    {
        $multipartStream = new MultipartStream($user->getMultipartArray());
        $request = $this->requestFactory
            ->createRequest('POST', self::CREATE_USER_ENDPOINT)
            ->withAddedHeader('Content-Type', 'multipart/form-data; boundary="' . $multipartStream->getBoundary() . '"')
            ->withBody($multipartStream);

        $response = $this->httpClient->sendRequest($request);

        $body = (string)$response->getBody();
        $bodyFormatted = json_decode($body, true);
        if ($response->getStatusCode() !== 200 || $bodyFormatted['status'] !== 'ok') {
            throw new S2lowClientException($body, $response->getStatusCode());
        }
        return $bodyFormatted;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function getUser(int $id): S2lowUser
    {
        $request = $this->requestFactory->createRequest('GET', self::GET_USER_ENDPOINT . "?id=$id");
        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();
        $bodyFormatted = json_decode($body, true);

        if ($response->getStatusCode() !== 200) {
            throw new S2lowClientException($body, $response->getStatusCode());
        }
        return S2lowUser::hydrate($bodyFormatted, $id);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function createOrUpdateAuthority(S2lowAuthority $authority): array
    {
        $request = $this->requestFactory
            ->createRequest('POST', self::CREATE_AUTHORITY_ENDPOINT)
            ->withBody($this->streamFactory->createStream(http_build_query($authority->toArray())));

        $response = $this->httpClient->sendRequest($request);

        $body = (string)$response->getBody();
        $bodyFormatted = json_decode($body, true);
        if ($response->getStatusCode() !== 200 || $bodyFormatted['status'] !== 'ok') {
            throw new S2lowClientException($body, $response->getStatusCode());
        }
        return $bodyFormatted;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function getAuthority(int $id): S2lowAuthority
    {
        $request = $this->requestFactory->createRequest('GET', self::GET_AUTHORITY_ENDPOINT . "?id=$id");
        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();
        $bodyFormatted = json_decode($body, true);

        if ($response->getStatusCode() !== 200) {
            throw new S2lowClientException($body, $response->getStatusCode());
        }

        return S2lowAuthority::hydrate($bodyFormatted);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function getAuthorityServices(int $authorityId): array
    {
        $request = $this->requestFactory->createRequest(
            'GET',
            self::GET_AUTHORITY_SERVICES_ENDPOINT . "?authority_id=$authorityId"
        );
        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();
        $bodyFormatted = json_decode($body, true);

        if ($response->getStatusCode() !== 200) {
            throw new S2lowClientException($body, $response->getStatusCode());
        }

        return $bodyFormatted;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function createAuthorityService(int $authorityId, string $name): array
    {
        $request = $this->requestFactory->createRequest(
            'POST',
            self::CREATE_AUTHORITY_SERVICE_ENDPOINT . "?authority_id=$authorityId"
        )->withBody($this->streamFactory->createStream(http_build_query([
            'api' => 1,
            'authority_id' => $authorityId,
            'name' => $name,
        ])));
        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();
        $bodyFormatted = json_decode($body, true);

        if ($response->getStatusCode() !== 200 || $bodyFormatted['status'] !== 'ok') {
            throw new S2lowClientException($body, $response->getStatusCode());
        }

        return $bodyFormatted;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function addUserToService(int $userId, int $serviceId): array
    {
        $request = $this->requestFactory->createRequest(
            'POST',
            self::ADD_USER_TO_SERVICE_ENDPOINT
        )->withBody($this->streamFactory->createStream(http_build_query([
            'api' => 1,
            'id_service' => $serviceId,
            'id_user' => $userId,
        ])));
        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();
        $bodyFormatted = json_decode($body, true);

        if ($response->getStatusCode() !== 200 || $bodyFormatted['status'] !== 'ok') {
            throw new S2lowClientException($body, $response->getStatusCode());
        }

        return $bodyFormatted;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function addSirenToGroup(int $groupeId, string $siren): bool
    {
        $request = $this->requestFactory->createRequest(
            'POST',
            self::ADD_SIREN_TO_GROUP_ENDPOINT
        )->withBody($this->streamFactory->createStream(http_build_query([
            'siren' => $siren,
            'id' => $groupeId,
        ])));

        return $this->httpClient->sendRequest($request)->getStatusCode() === 301;
    }
}
