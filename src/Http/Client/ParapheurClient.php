<?php

namespace App\Http\Client;

use Http\Discovery\Psr17FactoryDiscovery;
use Http\Discovery\Psr18ClientDiscovery;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use Psr\Http\Message\RequestFactoryInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\StreamFactoryInterface;

class ParapheurClient
{
    /** @var string */
    public const CONTENT_TYPE = 'application/json';

    /** @var string */
    public const LOGIN_ENDPOINT = '/api/login';
    /** @var string */
    public const USER_ENDPOINT = '/parapheur/utilisateurs';
    /** @var string */
    public const GROUP_ENDPOINT = '/parapheur/groupes';
    /** @var string */
    public const TENANT_ENDPOINT = '/parapheur/mc';

    /**
     * @var string
     */
    private $baseApi = '';

    /**
     * @var ClientInterface
     */
    private $httpClient;

    /**
     * @var RequestFactoryInterface
     */
    private $requestFactory;

    /**
     * @var StreamFactoryInterface
     */
    private $streamFactory;

    /**
     * @var string
     */
    private $username;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $accessTicket;


    public function __construct(
        ClientInterface $httpClient = null,
        RequestFactoryInterface $requestFactory = null,
        StreamFactoryInterface $streamFactory = null
    ) {
        $this->httpClient = $httpClient ?? Psr18ClientDiscovery::find();
        $this->requestFactory = $requestFactory ?? Psr17FactoryDiscovery::findRequestFactory();
        $this->streamFactory = $streamFactory ?? Psr17FactoryDiscovery::findStreamFactory();
    }

    public function setBaseApi(string $baseApi): void
    {
        $this->baseApi = $baseApi;
    }

    public function setCredentials(string $username, string $password): void
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function getAccessTicket(): string
    {
        if (empty($this->accessTicket)) {
            $this->requestAccessTicket();
        }

        return $this->accessTicket;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    private function requestAccessTicket(): void
    {
        $endpoint = $this->baseApi . self::LOGIN_ENDPOINT;

        $data = [
            'username' => $this->username,
            'password' => $this->password
        ];

        $request = $this->requestFactory
            ->createRequest('POST', $endpoint)
            ->withAddedHeader('Content-Type', self::CONTENT_TYPE)
            ->withBody($this->streamFactory->createStream(json_encode($data)));
        $response = $this->httpClient->sendRequest($request);

        $body = (string)$response->getBody();
        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        $content = json_decode($body, true);
        $this->accessTicket = $content['data']['ticket'];
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function createUser(
        string $username,
        string $firstname,
        string $lastname,
        string $email,
        string $password
    ): array {
        $data = [
            'username' => $username,
            'firstName' => $firstname,
            'lastName' => $lastname,
            'email' => $email,
            'password' => $password
        ];

        $response = $this->post(self::USER_ENDPOINT, $data);
        $body = (string)$response->getBody();

        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        $decodedBody = json_decode($body, true);
        if (
            !$decodedBody['id'] ||
            !preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $decodedBody['id'])
        ) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        return $decodedBody;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function updateUser(string $id, string $firstname, string $lastname, string $email): bool
    {
        $data = [
            'firstName' => $firstname,
            'lastName' => $lastname,
            'email' => $email
        ];
        $request = $this->requestFactory
            ->createRequest('PUT', $this->getUrl(self::USER_ENDPOINT . "/$id"))
            ->withAddedHeader('Content-Type', self::CONTENT_TYPE)
            ->withBody($this->streamFactory->createStream(json_encode($data)));

        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();

        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        return true;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function deleteUser(string $id)
    {
        $request = $this->requestFactory
            ->createRequest('DELETE', $this->getUrl(self::USER_ENDPOINT . "/$id"))
            ->withAddedHeader('Content-Type', self::CONTENT_TYPE);

        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();

        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        return json_decode($body, true);
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function getGroupByName(string $id): ?string
    {
        $request = $this->requestFactory
            ->createRequest('GET', $this->getUrl(self::GROUP_ENDPOINT));

        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();
        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        $decodedBody = json_decode($body, true);
        foreach ($decodedBody as $group) {
            if ($group['shortName'] === $id) {
                return $group['id'];
            }
        }
        return null;
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function createGroup(string $name): array
    {
        $data = [
            'shortName' => $name,
        ];

        $response = $this->post(self::GROUP_ENDPOINT, $data);
        $body = (string)$response->getBody();
        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        return json_decode($body, true);
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function addUserToGroup(string $groupId, string $username)
    {
        $request = $this->requestFactory
            ->createRequest('POST', $this->getUrl(self::GROUP_ENDPOINT . "/$groupId/$username"));

        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();

        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        return json_decode($body, true);
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function deleteGroup(string $id): bool
    {
        $request = $this->requestFactory
            ->createRequest('DELETE', $this->getUrl(self::GROUP_ENDPOINT . "/$id"));

        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();

        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        return true;
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function getTenants(): array
    {
        $request = $this->requestFactory
            ->createRequest('GET', $this->getUrl(self::TENANT_ENDPOINT));

        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();
        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        return json_decode($body, true);
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function tenantExists(string $tenantName): bool
    {
        $tenants = $this->getTenants();
        return in_array($tenantName, array_column($tenants, 'tenantDomain'), true);
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function createTenant(string $tenantName, string $title, string $description, string $adminPassword): bool
    {
        $data = [
            'title' => $title,
            'description' => $description,
            'password' => $adminPassword,
        ];

        $request = $this->requestFactory
            ->createRequest('POST', $this->getUrl(self::TENANT_ENDPOINT . "/$tenantName"))
            ->withAddedHeader('Content-Type', self::CONTENT_TYPE)
            ->withBody($this->streamFactory->createStream(json_encode($data)));

        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();
        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }

        // For some reasons, the parapheur can respond a success with an error at creation in its logs
        if (!$this->tenantExists($tenantName)) {
            throw new ParapheurClientException(
                "Unable to create the tenant, check the logs on the parapheur",
                $response->getStatusCode()
            );
        }
        return true;
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function toggleTenant(string $tenantName, bool $enable): bool
    {
        $data = [
            'enabled' => $enable,
        ];

        $request = $this->requestFactory
            ->createRequest('POST', $this->getUrl(self::TENANT_ENDPOINT . "/$tenantName/enable"))
            ->withAddedHeader('Content-Type', self::CONTENT_TYPE)
            ->withBody($this->streamFactory->createStream(json_encode($data)));

        $response = $this->httpClient->sendRequest($request);
        $body = (string)$response->getBody();

        if ($response->getStatusCode() !== 200) {
            throw new ParapheurClientException($body, $response->getStatusCode());
        }
        return true;
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function enableTenant(string $tenantName): bool
    {
        return $this->toggleTenant($tenantName, true);
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function disableTenant(string $tenantName): bool
    {
        return $this->toggleTenant($tenantName, false);
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    private function getUrl(string $endpoint): string
    {
        return $this->baseApi . $endpoint . '?' .
            http_build_query(
                ['ticket' => $this->getAccessTicket()]
            );
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    private function post(string $endpoint, array $data): ResponseInterface
    {
        $request = $this->requestFactory
            ->createRequest('POST', $this->getUrl($endpoint))
            ->withAddedHeader('Content-Type', self::CONTENT_TYPE)
            ->withBody($this->streamFactory->createStream(json_encode($data)));

        return $this->httpClient->sendRequest($request);
    }
}
