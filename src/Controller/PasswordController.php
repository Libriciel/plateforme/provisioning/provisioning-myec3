<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class PasswordController extends AbstractController
{

    /**
     * @Route("/password", name="app_password_index", methods={"GET", "POST"})
     */
    public function index(Request $request): Response
    {
        $password = '';
        $secretKey = $request->get('key');
        $socleId = $request->get('id');
        if ($secretKey && $socleId) {
            $password = hash_hmac('sha256', $socleId, $secretKey);
        }

        return $this->render('password/index.html.twig', [
            'password' => $password
        ]);
    }
}
