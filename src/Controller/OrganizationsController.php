<?php

namespace App\Controller;

use App\Enum\ProductEnum;
use App\Exception\SocleException;
use App\Factory\SocleFactoryProducer;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route(
 *     "/{product}/{number}",
 *     requirements={
 *       "product": ProductEnum::ORGANIZATION_REGEX,
 *       "number": "[1-9]\d*"
 *     }
 * )
 */
class OrganizationsController
{
    /**
     * @var LoggerInterface
     */
    private $logger;
    /**
     * @var SocleFactoryProducer
     */
    private $socleFactoryProducer;

    public function __construct(LoggerInterface $logger, SocleFactoryProducer $socleFactoryProducer)
    {
        $this->logger = $logger;
        $this->socleFactoryProducer = $socleFactoryProducer;
    }

    /**
     * @Route("/organism", name="create_organization", methods={"POST"})
     */
    public function post(
        string $product,
        int $number,
        Request $request
    ): Response {
        $xml = $request->getContent();
        try {
            $socle = $this->socleFactoryProducer->create($product, $number);
            $socle->addStructureXml($xml);
        } catch (\Exception $e) {
            $this->logger->error($xml);
            $this->logger->error($e->getMessage(), $e->getTrace());
            if ($e instanceof SocleException) {
                $response = new Response($e->getMessage(), $e->getCode());
            } else {
                $socleException = new SocleException(
                    SocleException::ORGANISM,
                    '006',
                    'INTERNAL_SERVER_ERROR',
                    $e->getMessage(),
                    'POST',
                    '',
                    500
                );
                $response = new Response($socleException->getMessage(), $socleException->getCode());
            }
            $response->headers->set('Content-Type', 'application/xml');
            return $response;
        }
        return new Response('', Response::HTTP_CREATED);
    }

    /**
     * @Route("/organism/{id}", name="update_organization", methods={"PUT"})
     */
    public function put(
        string $product,
        int $number,
        string $id,
        Request $request
    ): Response {
        $xml = $request->getContent();
        try {
            $socle = $this->socleFactoryProducer->create($product, $number);
            $xmlObject = @simplexml_load_string($xml);
            if ($xmlObject) {
                $id = (string)$xmlObject->externalId ?? $id;
            }
            $socle->updateStructureXml($xml, $id);
        } catch (\Exception $e) {
            $this->logger->error($xml);
            $this->logger->error($e->getMessage(), $e->getTrace());
            if ($e instanceof SocleException) {
                $response = new Response($e->getMessage(), $e->getCode());
            } else {
                $socleException = new SocleException(
                    SocleException::ORGANISM,
                    '006',
                    'INTERNAL_SERVER_ERROR',
                    $e->getMessage(),
                    'PUT',
                    $id,
                    500
                );
                $response = new Response($socleException->getMessage(), $socleException->getCode());
            }
            $response->headers->set('Content-Type', 'application/xml');
            return $response;
        }
        return new Response('', Response::HTTP_OK);
    }

    /**
     * @Route("/organism/{id}", name="delete_organization", methods={"DELETE"})
     */
    public function delete(
        string $product,
        int $number,
        string $id
    ): Response {
        try {
            $socle = $this->socleFactoryProducer->create($product, $number);
            $socle->deleteStructure($id);
        } catch (\Exception $e) {
            $this->logger->error($e->getMessage(), $e->getTrace());
            if ($e instanceof SocleException) {
                $response = new Response($e->getMessage(), $e->getCode());
            } else {
                $socleException = new SocleException(
                    SocleException::ORGANISM,
                    '006',
                    'INTERNAL_SERVER_ERROR',
                    $e->getMessage(),
                    'DELETE',
                    $id,
                    500
                );
                $response = new Response($socleException->getMessage(), $socleException->getCode());
            }
            $response->headers->set('Content-Type', 'application/xml');
            return $response;
        }
        return new Response('', Response::HTTP_OK);
    }
}
