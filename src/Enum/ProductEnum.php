<?php

namespace App\Enum;

/**
 * TODO: PHP 8.1 => replace by an enum
 */
final class ProductEnum
{
    public const S2LOW = 's2low';
    public const PASTELL = 'pastell';
    public const PARAPHEUR_MONOTENANT = 'parapheur';
    public const PARAPHEUR_MULTITENANT = 'parapheur_multi';

    public const AGENT_REGEX = '(' . self::PASTELL . '|' . self::PARAPHEUR_MONOTENANT . '|' . self::S2LOW . '|' . self::PARAPHEUR_MULTITENANT . ')';
    public const ORGANIZATION_REGEX = '(' . self::PASTELL . '|' . self::PARAPHEUR_MONOTENANT . '|' . self::S2LOW . '|' . self::PARAPHEUR_MULTITENANT . ')';
    public const DEPARTMENT_REGEX = '(' . self::PASTELL . ')';
}
