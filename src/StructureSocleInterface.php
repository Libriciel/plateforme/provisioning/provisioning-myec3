<?php
/**
 * Created by PhpStorm.
 * User: rdubourget
 * Date: 04/11/19
 * Time: 17:05
 */

namespace App;

use SimpleXMLElement;

interface StructureSocleInterface
{
    /**
     * Add structure
     * @param SimpleXMLElement $xml
     * @return bool
     */
    public function add(SimpleXMLElement $xml): bool;

    /**
     * update a structure
     * @param SimpleXMLElement $xml
     * @param string|int $structureId
     * @return bool
     */
    public function update(SimpleXMLElement $xml, $structureId): bool;

    /**
     * @param string|int $structureId
     * @return bool
     */
    public function delete($structureId): bool;

    /**
     * get structureId
     * @param string $externalId
     * @return string|int|null
     */
    public function getId(string $externalId);

    /**
     * get role Id
     * @param string|int $roleName
     * @param string|int $structureId
     * @return string|int|null
     */
    public function getRoleId($roleName, $structureId);
}
