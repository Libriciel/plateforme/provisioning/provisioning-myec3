FROM php:7.2-cli

RUN apt-get update && apt-get install -y \
    git \
    zip

COPY --from=composer:2 /usr/bin/composer /usr/bin/composer

RUN  curl -1sLf 'https://dl.cloudsmith.io/public/symfony/stable/setup.deb.sh' | bash && \
    apt-get install symfony-cli

RUN pecl install pcov

RUN docker-php-ext-install pdo_mysql && docker-php-ext-enable pcov

WORKDIR /app/
COPY ./ /app/

RUN COMPOSER_MEMORY_LIMIT=-1 composer install

CMD [ "symfony", "serve", "--port=8000"]
