<?php

namespace App\Tests;

use App\DepartmentSocleInterface;
use App\Socle;
use App\Exception\SocleAlreadyExitsRessourceException;
use App\Exception\SocleInternalErrorException;
use App\Exception\SocleMissingRessourceException;
use App\Exception\SocleRelationMissingException;
use App\Exception\SocleSyntaxException;
use App\StructureSocleInterface;
use App\UserSocleInterface;
use PHPUnit\Framework\TestCase;

class Myec3Test extends TestCase
{
    const XML = __DIR__ . "/xml/";

    protected function mockUser()
    {
        $user = $this->createMock(UserSocleInterface::class);
        return $user;
    }

    protected function mockUserNew()
    {
        $user = $this->createMock(UserSocleInterface::class);
        $user->method('getId')->willReturn(false);
        $user->method('add')->willReturn(true);
        return $user;
    }

    protected function mockUserExists()
    {
        $user = $this->createMock(UserSocleInterface::class);
        $user->method('getId')->willReturn(true);
        $user->method('add')->willReturn(true);
        $user->method('update')->willReturn(true);
        $user->method('delete')->willReturn(true);
        return $user;
    }

    protected function mockStructure()
    {
        return $this->createMock(StructureSocleInterface::class);
    }

    protected function mockStructureBadStructure()
    {
        $structure = $this->createMock(StructureSocleInterface::class);
        $structure->method('update')->willReturn(true);
        $structure->method('getId')->willReturn(false);
        $structure->method('getRoleId')->willReturn(1);

        return $structure;
    }

    protected function mockStructureAlreadyExist()
    {
        $structure = $this->createMock(StructureSocleInterface::class);
        $structure->method('update')->willReturn(true);
        $structure->method('getId')->willReturn(1);
        $structure->method('getRoleId')->willReturn(1);
        $structure->method('delete')->willReturn(true);

        return $structure;
    }

    protected function mockStructureNew()
    {
        $structure = $this->createMock(StructureSocleInterface::class);
        $structure->method('getId')->willReturn(false);
        $structure->method('add')->willReturn(true);

        return $structure;
    }

    protected function mockStructureDeleteError()
    {
        $structure = $this->createMock(StructureSocleInterface::class);
        $structure->method('getId')->willReturn(true);
        $structure->method('delete')->willReturn(false);

        return $structure;
    }

    protected function mockStructureAddError()
    {
        $structure = $this->createMock(StructureSocleInterface::class);
        $structure->method('getId')->willReturn(false);
        $structure->method('add')->willReturn(false);

        return $structure;
    }


    protected function mockDepartment()
    {
        return $this->createMock(DepartmentSocleInterface::class);
    }

    protected function mockDepartmentNew()
    {
        $user = $this->mockDepartment();
        $user->method('getId')->willReturn(false);
        $user->method('add')->willReturn(true);
        return $user;
    }

    protected function mockDepartmentAlreadyExists()
    {
        $department = $this->mockDepartment();
        $department->method('getId')->willReturn(42);
        $department->method('delete')->willReturn(true);
        return $department;
    }



    public function testAddStructureXml()
    {
        $xml = file_get_contents(self::XML . 'organism.xml');
        $this->assertNotEmpty($xml);
        $user = $this->mockUser();
        $structure = $this->mockStructureNew();
        $socle = new Socle($user, $structure);
        $res = $socle->addStructureXml($xml);

        $this->assertNotEmpty($res);
    }


    public function testAddStructureXmlAlreadyExists()
    {
        $xml = file_get_contents(self::XML . 'organism.xml');
        $this->assertNotEmpty($xml);

        $user = $this->mockUser();
        $structure = $this->mockStructureAlreadyExist();
        $socle = new Socle($user, $structure);
        try {
            $res = $socle->addStructureXml($xml);
        } catch (SocleAlreadyExitsRessourceException $e) {
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>ORGANISM</classType><errorCode>005</errorCode><errorLabel>RESOURCE_ALREADY_EXISTS</errorLabel><errorMessage>La resource existe déjà</errorMessage><methodType>POST</methodType><resourceId>100000005</resourceId></error>
';
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals(409, $e->getCode());
        }
    }


    public function testAddStructureXmlAddFailed()
    {
        $xml = file_get_contents(self::XML . 'organism.xml');
        $this->assertNotEmpty($xml);

        $user = $this->mockUser();
        $structure = $this->mockStructureAddError();
        $socle = new Socle($user, $structure);
        try {
            $socle->addStructureXml($xml);
        } catch (SocleInternalErrorException $e) {
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>ORGANISM</classType><errorCode>006</errorCode><errorLabel>INTERNAL_SERVER_ERROR</errorLabel><errorMessage>Erreur interne au serveur</errorMessage><methodType>PUT</methodType><resourceId>100000005</resourceId></error>
';
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals(500, $e->getCode());
        }
    }


    public function testUpdateStructureXml()
    {
        $xml = file_get_contents(self::XML . 'organism.xml');
        $this->assertNotEmpty($xml);
        $user = $this->mockUser();
        $structure = $this->mockStructureAlreadyExist();
        $socle = new Socle($user, $structure);
        $res = $socle->updateStructureXml($xml, 1);

        $this->assertNotEmpty($res);
    }


    public function testUpdateStructureXmlNotExists()
    {
        $xml = file_get_contents(self::XML . 'organism.xml');
        $this->assertNotEmpty($xml);
        $user = $this->mockUser();
        $structure = $this->mockStructureNew();
        $socle = new Socle($user, $structure);
        $res = $socle->updateStructureXml($xml, 1);

        $this->assertNotEmpty($res);
    }

    public function testAddUser()
    {
        $xml = file_get_contents(self::XML . 'agent.xml');
        $this->assertNotEmpty($xml);
        $user = $this->mockUserNew();
        $structure = $this->mockStructureAlreadyExist();
        $socle = new Socle($user, $structure);
        $res = $socle->addUserXml($xml);

        $this->assertNotEmpty($res);
    }

    public function testAddUserWhenDepartementEntityExits()
    {
        $xml = file_get_contents(self::XML . 'agent.xml');
        $this->assertNotEmpty($xml);
        $user = $this->mockUserNew();
        $structure = $this->mockStructureAlreadyExist();
        $departement = $this->mockDepartmentAlreadyExists();
        $socle = new Socle($user, $structure,$departement);
        $res = $socle->addUserXml($xml);
        $this->assertNotEmpty($res);
    }

    public function testAddUserWhenDepartementEntityExitsButNotDepartement()
    {
        $xml = file_get_contents(self::XML . 'agent.xml');
        $this->assertNotEmpty($xml);
        $user = $this->mockUserNew();
        $structure = $this->mockStructureAlreadyExist();
        $departement = $this->mockDepartment();
        $socle = new Socle($user, $structure,$departement);
        try {
            $socle->addUserXml($xml);
        } catch (SocleRelationMissingException $e){
            $this->assertXmlStringEqualsXmlFile(
                __DIR__ . "/fixtures/add_user_with_departement_entity_no_departement_found.xml",
                $e->getMessage()
            );
        }
    }

    public function testAddUserAlreadyExists()
    {
        $xml = file_get_contents(self::XML . 'agent.xml');
        $this->assertNotEmpty($xml);
        $user = $this->mockUserExists();
        $structure = $this->mockStructureAlreadyExist();
        $socle = new Socle($user, $structure);
        try {
            $res = $socle->addUserXml($xml);
        }catch (SocleAlreadyExitsRessourceException $e) {
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>AGENT_PROFILE</classType><errorCode>005</errorCode><errorLabel>RESOURCE_ALREADY_EXISTS</errorLabel><errorMessage>La resource existe déjà</errorMessage><methodType>POST</methodType><resourceId>300005473</resourceId></error>
';
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals(409, $e->getcode());
        }

    }

    public function testAddUserRelationMissing()
    {
        $xml = file_get_contents(self::XML . 'agent.xml');
        $this->assertNotEmpty($xml);
        $user = $this->mockUserNew();
        $structure = $this->mockStructureBadStructure();
        $socle = new Socle($user, $structure);
        try {
            $res = $socle->addUserXml($xml);
        }catch(SocleRelationMissingException $e){
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>ORGANISM</classType><errorCode>004</errorCode><errorLabel>RELATION_MISSING</errorLabel><errorMessage>Une relation avec l\'entité est manquante</errorMessage><methodType>POST</methodType><resourceId>100000005</resourceId></error>
';
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals(400, $e->getCode());
        }
    }

    /**
     * @throws SocleRelationMissingException
     * @throws SocleInternalErrorException
     * @throws SocleSyntaxException
     * @throws SocleAlreadyExitsRessourceException
     */
    public function testUpdateUser(): void
    {
        $xml = file_get_contents(self::XML . 'agent.xml');
        $this->assertNotEmpty($xml);
        $user = $this->mockUserExists();
        $structure = $this->mockStructureAlreadyExist();
        $department = $this->mockDepartmentAlreadyExists();
        $socle = new Socle($user, $structure, $department);
        $res = $socle->updateUserXml($xml, 1);

        $this->assertNotEmpty($res);
    }


    public function testbadSyntaxXml(){
        $xml = file_get_contents(self::XML . 'bad.xml');
        $this->assertNotEmpty($xml);
        $user = $this->mockUserExists();
        $structure = $this->mockStructureAlreadyExist();
        $socle = new Socle($user, $structure);
        try {
            $res = $socle->updateUserXml($xml, 1);
        }catch(SocleSyntaxException $e){
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>unknown</classType><errorCode>001</errorCode><errorLabel>SYNTAX_ERROR</errorLabel><errorMessage>Syntaxe du message en entrée incorrecte</errorMessage><methodType>unknown</methodType><resourceId>unknown</resourceId></error>
';
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals(400, $e->getCode());
        }
    }


    public function testDeleteStructure()
    {
        $user = $this->mockUserExists();
        $structure = $this->mockStructureAlreadyExist();
        $socle = new Socle($user, $structure);
        $res = $socle->deleteStructure(1);
        $this->assertNotEmpty($res);
    }

    public function testDeleteStructureNotExists()
    {
        $user = $this->mockUserExists();
        $structure = $this->mockStructureNew();
        $socle = new Socle($user, $structure);
        try {
            $res = $socle->deleteStructure(1);
        }catch(SocleMissingRessourceException $e){
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>ORGANISM</classType><errorCode>003</errorCode><errorLabel>RESOURCE_MISSING</errorLabel><errorMessage>La ressource spécifiée est introuvable</errorMessage><methodType>DELETE</methodType><resourceId>1</resourceId></error>
';
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals(404, $e->getCode());
        }
    }

    public function testDeleteStructureError(){
        $user = $this->mockUserExists();
        $structure = $this->mockStructureDeleteError();
        $socle = new Socle($user, $structure);
        try {
            $res = $socle->deleteStructure(1);
        }catch(SocleInternalErrorException $e){
            $expected = '<?xml version="1.0" encoding="UTF-8"?>
<error xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:noNamespaceSchemaLocation="error.xsd"><classType>ORGANISM</classType><errorCode>006</errorCode><errorLabel>INTERNAL_SERVER_ERROR</errorLabel><errorMessage>Erreur interne au serveur</errorMessage><methodType>DELETE</methodType><resourceId>1</resourceId></error>
';
            $this->assertEquals($expected, $e->getMessage());
            $this->assertEquals(500, $e->getCode());
        }
    }


    public function testDeleteUser()
    {
        $user = $this->mockUserExists();
        $structure = $this->mockStructureAlreadyExist();
        $socle = new Socle($user, $structure);
        $res = $socle->deleteUser(1);
        $this->assertNotEmpty($res);
    }

    /**
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleInternalErrorException
     * @throws SocleSyntaxException
     */
    public function testAddDepartment()
    {
        $xml = file_get_contents(self::XML . 'department.xml');
        $this->assertNotEmpty($xml);
        $department = $this->mockDepartmentNew();
        $structure = $this->mockStructureAlreadyExist();

        $socle = new Socle(null, $structure,$department);
        $res = $socle->addDepartmentXml($xml);
        $this->assertNotEmpty($res);
    }

    /**
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleInternalErrorException
     * @throws SocleSyntaxException
     */
    public function testAddDepartmentAlreadyExists()
    {
        $xml = file_get_contents(self::XML . 'department.xml');
        $this->assertNotEmpty($xml);
        $department = $this->mockDepartmentAlreadyExists();
        $structure = $this->mockStructureAlreadyExist();

        $socle = new Socle(null, $structure,$department);
        try {
            $socle->addDepartmentXml($xml);
            $this->assertFalse(true);
        } catch (SocleAlreadyExitsRessourceException $e){
            $this->assertStringEqualsFile(
                __DIR__."/fixtures/add_departement_already_existes_exception.xml",
                $e->getMessage()
            );
        }
    }

    /**
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleInternalErrorException
     * @throws SocleSyntaxException
     */
    public function testAddDepartmentOrganismDoNotExists()
    {
        $xml = file_get_contents(self::XML . 'department.xml');
        $this->assertNotEmpty($xml);
        $department = $this->mockDepartment();
        $structure = $this->mockStructure();

        $socle = new Socle(null, $structure,$department);
        try {
            $socle->addDepartmentXml($xml);
            $this->assertFalse(true);
        } catch (SocleRelationMissingException $e){
            $this->assertXmlStringEqualsXmlFile(
                __DIR__."/fixtures/add_departement_organism_dont_exists_exception.xml",
                $e->getMessage()
            );
        }
    }

    /**
     * @throws SocleAlreadyExitsRessourceException
     * @throws SocleInternalErrorException
     * @throws SocleSyntaxException
     */
    public function testAddDepartmentParentDontExists()
    {
        $xml = file_get_contents(self::XML . 'department2.xml');
        $this->assertNotEmpty($xml);
        $department = $this->mockDepartment();
        $structure = $this->mockStructureAlreadyExist();

        $socle = new Socle(null, $structure,$department);
        try {
            $socle->addDepartmentXml($xml);
            $this->assertFalse(true);
        } catch (SocleRelationMissingException $e){
            $this->assertXmlStringEqualsXmlFile(
                __DIR__."/fixtures/add_departement_department_dont_exists_exception.xml",
                $e->getMessage()
            );
        }
    }

    public function testUpdateDepartement(){
        $xml = file_get_contents(self::XML . 'department.xml');
        $department = $this->mockDepartmentNew();
        $structure = $this->mockStructureAlreadyExist();
        $socle = new Socle(null, $structure,$department);
        $res = $socle->updateDepartmentXml($xml,1096);
        $this->assertNotEmpty($res);
    }

    public function testDeleteDepartment(){
        $department = $this->mockDepartmentAlreadyExists();
        $structure = $this->mockStructureAlreadyExist();
        $socle = new Socle(null, $structure,$department);
        $res = $socle->deleteDepartment(1096);
        $this->assertNotEmpty($res);
    }
}
