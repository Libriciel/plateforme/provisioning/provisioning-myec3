<?php

namespace App\Tests\Product\ParapheurMultitenant;

use App\Entity\ProductUserSocle;
use App\Enum\ProductEnum;
use App\Http\Client\ParapheurClient;
use App\Http\Client\ParapheurClientException;
use App\Product\ParapheurMultitenant\UserProvisioning;
use App\Repository\ProductUserSocleRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class UserProvisioningTest extends TestCase
{
    private function getProductUserSocle(
        string $product,
        int $number,
        string $socleId,
        string $productId
    ): ProductUserSocle {
        $productUserSocle = new ProductUserSocle();
        $productUserSocle->setProductId($product);
        $productUserSocle->setProductNumber($number);
        $productUserSocle->setUserSocleId($socleId);
        $productUserSocle->setUserProductId($productId);
        return $productUserSocle;
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testAdd(): void
    {
        $productUserSocle = $this->getProductUserSocle(
            ProductEnum::PARAPHEUR_MULTITENANT,
            1,
            '300005473',
            'uuid@tenant.test'
        );

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('add')
            ->with($productUserSocle)
            ->willReturn(true);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $parapheurClient
            ->expects($this->once())
            ->method('createUser')
            ->willReturn([
                'id' => 'uuid',
            ]);

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MULTITENANT,
            1,
            'password'
        );
        $this->assertTrue(
            $provisioningUser->add(
                simplexml_load_string(file_get_contents(__DIR__ . '/../../fixtures/agent.xml')),
                0,
                ""
            )
        );
    }

    public function testUpdate()
    {
        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $parapheurClient
            ->expects($this->once())
            ->method('updateUser');

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MULTITENANT,
            1,
            'password'
        );
        $this->assertTrue(
            $provisioningUser->update(
                simplexml_load_string(file_get_contents(__DIR__ . '/../../fixtures/agent.xml')), 123, 111, "")
        );
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testDelete()
    {
        $productUserSocle = $this->getProductUserSocle(
            ProductEnum::PARAPHEUR_MULTITENANT,
            1,
            '300005473',
            'uuid@tenant.test'
        );

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('getByUserProductId')
            ->with(ProductEnum::PARAPHEUR_MULTITENANT, 1, 'uuid@tenant.test')
            ->willReturn($productUserSocle);
        $productUserSocleRepository
            ->expects($this->once())
            ->method('delete')
            ->willReturn(true);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $parapheurClient
            ->expects($this->once())
            ->method('deleteUser');

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MULTITENANT,
            1,
            'password'
        );
        $this->assertTrue($provisioningUser->delete('uuid@tenant.test'));
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \App\Exception\ProductUserSocleNotFoundException
     */
    public function testGetId()
    {
        $productUserSocle = $this->getProductUserSocle(
            ProductEnum::PARAPHEUR_MULTITENANT,
            1,
            '300005473',
            'uuid@tenant.test'
        );
        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $productUserSocleRepository
            ->expects($this->once())
            ->method('get')
            ->with(ProductEnum::PARAPHEUR_MULTITENANT, 1, '300005473')
            ->willReturn($productUserSocle);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MULTITENANT,
            1,
            'password'
        );
        $this->assertEquals('uuid@tenant.test', $provisioningUser->getId('300005473'));
    }
}
