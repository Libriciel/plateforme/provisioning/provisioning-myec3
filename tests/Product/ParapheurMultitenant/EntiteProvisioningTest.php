<?php

namespace App\Tests\Product\ParapheurMultitenant;

use App\Entity\ProductOrganizationSocle;
use App\Enum\ProductEnum;
use App\Http\Client\ParapheurClient;
use App\Product\ParapheurMultitenant\EntiteProvisioning;
use App\Repository\ProductOrganizationSocleRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class EntiteProvisioningTest extends TestCase
{

    private function getProductOrganizationSocle(
        string $product,
        int $number,
        string $socleId,
        string $productId
    ): ProductOrganizationSocle {
        $productOrganizationSocle = new ProductOrganizationSocle();
        $productOrganizationSocle->setProductId($product);
        $productOrganizationSocle->setProductNumber($number);
        $productOrganizationSocle->setOrganizationSocleId($socleId);
        $productOrganizationSocle->setOrganizationProductId($productId);
        return $productOrganizationSocle;
    }

    /**
     * @throws \App\Http\Client\ParapheurClientException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function testAdd()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            ProductEnum::PARAPHEUR_MULTITENANT,
            1,
            '100000005',
            'tenant.10'
        );

        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('add')
            ->with($productOrganizationSocle)
            ->willReturn(true);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $parapheurClient
            ->expects($this->once())
            ->method('getTenants')
            ->willReturn([]);
        $parapheurClient
            ->expects($this->once())
            ->method('createTenant')
            ->willReturn(true);

        $entiteProvisioning = new EntiteProvisioning(
            $productOrganizationSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MULTITENANT,
            1,
            'password'
        );
        $this->assertTrue(
            $entiteProvisioning->add(
                simplexml_load_string(file_get_contents(__DIR__ . '/../../fixtures/organism.xml'))
            )
        );
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \App\Http\Client\ParapheurClientException
     * @throws \Psr\Http\Client\ClientExceptionInterface
     */
    public function testDelete()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            ProductEnum::PARAPHEUR_MULTITENANT,
            1,
            '100000005',
            'tenant.10'
        );

        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('getByOrganizationProductId')
            ->with(ProductEnum::PARAPHEUR_MULTITENANT, 1, 'tenant.10')
            ->willReturn($productOrganizationSocle);
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('delete')
            ->with($productOrganizationSocle)
            ->willReturn(true);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $parapheurClient
            ->expects($this->once())
            ->method('disableTenant')
            ->with('tenant.10')
            ->willReturn(true);

        $entiteProvisioning = new EntiteProvisioning(
            $productOrganizationSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MULTITENANT,
            1,
            'password'
        );

        $this->assertTrue($entiteProvisioning->delete('tenant.10'));
    }

    /**
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function testGetId()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            ProductEnum::PARAPHEUR_MONOTENANT,
            1,
            '100000005',
            'tenant.10'
        );
        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('get')
            ->with(ProductEnum::PARAPHEUR_MULTITENANT, 1, '100000005')
            ->willReturn($productOrganizationSocle);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();


        $provisioningUser = new EntiteProvisioning(
            $productOrganizationSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MULTITENANT,
            1,
            'password'
        );
        $this->assertEquals('tenant.10', $provisioningUser->getId('100000005'));
    }

}
