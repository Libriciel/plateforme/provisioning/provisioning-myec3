<?php

namespace App\Tests\Product\S2low;

use App\Entity\ProductUserSocle;
use App\Enum\ProductEnum;
use App\Exception\ProductUserSocleNotFoundException;
use App\Http\Client\S2lowClient;
use App\Http\Client\S2lowClientException;
use App\Http\Model\S2lowUser;
use App\Product\S2low\UserProvisioning;
use App\Repository\ProductUserSocleRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class UserProvisioningTest extends TestCase
{

    private function getProductUserSocle(
        string $product,
        int $number,
        string $socleId,
        string $productId
    ): ProductUserSocle {
        $productUserSocle = new ProductUserSocle();
        $productUserSocle->setProductId($product);
        $productUserSocle->setProductNumber($number);
        $productUserSocle->setUserSocleId($socleId);
        $productUserSocle->setUserProductId($productId);
        return $productUserSocle;
    }


    /**
     * @throws S2lowClientException
     * @throws ClientExceptionInterface
     */
    public function testAdd()
    {
        $productUserSocle = $this->getProductUserSocle(
            ProductEnum::S2LOW,
            1,
            '300005473',
            123
        );

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('add')
            ->with($productUserSocle)
            ->willReturn(true);

        /** @var MockObject|S2lowClient $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $s2lowClient
            ->expects($this->once())
            ->method('createOrUpdateUser')
            ->willReturn([
                'id' => 123,
            ]);

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $s2lowClient,
            ProductEnum::S2LOW,
            1,
            [
                'authority_group_id' => 1,
                'role' => 'role',
            ]
        );
        $this->assertTrue(
            $provisioningUser->add(
                simplexml_load_file(__DIR__ . '/../../fixtures/agent.xml'),
                0,
                ""
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testUpdate()
    {
        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|S2lowClient $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $s2lowClient
            ->expects($this->once())
            ->method('getUser')
            ->with(123)
            ->willReturn(S2lowUser::hydrate([
                'name' => 'Klaus',
                'givenname' => 'Heissler',
                'login' => 'socle_10',
                'email' => 'klausheissler@gmail.com',
                'authority_id' => 1,
                'status' => 1,
                'role' => 'role',
                'authority_group_id' => 1,
                'certificate' => '...',
            ], 123));

        $s2lowClient
            ->expects($this->once())
            ->method('getAuthorityServices')
            ->with(1)
            ->willReturn([
                [
                    'id' => 1
                ]
            ]);

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $s2lowClient,
            ProductEnum::S2LOW,
            1,
            [
                'authority_group_id' => 1,
                'role' => 'role',
            ]
        );
        $this->assertTrue(
            $provisioningUser->update(simplexml_load_file(__DIR__ . '/../../fixtures/agent.xml'), 123, 111, 1)
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testDelete()
    {
        $productUserSocle = $this->getProductUserSocle(
            ProductEnum::S2LOW,
            1,
            '300005473',
            123
        );
        $user = S2lowUser::hydrate(
            [
                'name' => 'Klaus',
                'givenname' => 'Heissler',
                'login' => 'socle_10',
                'email' => 'klausheissler@gmail.com',
                'authority_id' => 1,
                'status' => 1,
                'role' => 'role',
                'authority_group_id' => 1,
                'certificate' => '...',
            ],
            123
        );

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('getByUserProductId')
            ->with(ProductEnum::S2LOW, 1, 123)
            ->willReturn($productUserSocle);
        $productUserSocleRepository
            ->expects($this->once())
            ->method('delete')
            ->willReturn(true);

        /** @var MockObject|S2lowClient $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $s2lowClient
            ->expects($this->once())
            ->method('getUser')
            ->with(123)
            ->willReturn($user);

        $user->status = 0;

        $s2lowClient
            ->expects($this->once())
            ->method('createOrUpdateUser')
            ->with($user)
            ->willReturn([]);

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $s2lowClient,
            ProductEnum::S2LOW,
            1,
            [
                'authority_group_id' => 1,
                'role' => 'role',
            ]
        );
        $this->assertTrue($provisioningUser->delete(123));
    }

    /**
     * @throws ProductUserSocleNotFoundException
     */
    public function testGetId()
    {
        $productUserSocle = $this->getProductUserSocle(
            ProductEnum::S2LOW,
            1,
            7,
            123
        );

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $productUserSocleRepository
            ->expects($this->once())
            ->method('get')
            ->with(ProductEnum::S2LOW, 1, 7)
            ->willReturn($productUserSocle);

        /** @var MockObject|S2lowClient $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $s2lowClient,
            ProductEnum::S2LOW,
            1,
            []
        );
        $this->assertEquals(123, $provisioningUser->getId(7));
    }
}
