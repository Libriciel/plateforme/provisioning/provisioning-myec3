<?php

namespace App\Tests\Product\S2low;

use App\Entity\ProductOrganizationSocle;
use App\Enum\ProductEnum;
use App\Exception\ProductOrganizationSocleNotFoundException;
use App\Http\Client\S2lowClient;
use App\Http\Client\S2lowClientException;
use App\Http\Model\S2lowAuthority;
use App\Product\S2low\EntiteProvisioning;
use App\Repository\ProductOrganizationSocleRepository;
use App\Service\S2lowTechnicalUserService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class EntiteProvisioningTest extends TestCase
{

    private function getProductOrganizationSocle(
        string $product,
        int $number,
        string $socleId,
        string $productId
    ): ProductOrganizationSocle {
        $productOrganizationSocle = new ProductOrganizationSocle();
        $productOrganizationSocle->setProductId($product);
        $productOrganizationSocle->setProductNumber($number);
        $productOrganizationSocle->setOrganizationSocleId($socleId);
        $productOrganizationSocle->setOrganizationProductId($productId);
        return $productOrganizationSocle;
    }

    /**
     * @throws S2lowClientException
     * @throws ClientExceptionInterface
     */
    public function testAdd()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            ProductEnum::S2LOW,
            1,
            '100000005',
            123
        );

        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('add')
            ->with($productOrganizationSocle)
            ->willReturn(true);

        /** @var S2lowClient|MockObject $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $s2lowClient
            ->expects($this->once())
            ->method('createOrUpdateAuthority')
            ->willReturn([
                'id' => 123,
            ]);

        $s2lowTechnicalUserService = $this->createMock(S2lowTechnicalUserService::class);

        $entiteProvisioning = new EntiteProvisioning(
            $productOrganizationSocleRepository,
            $s2lowClient,
            $s2lowTechnicalUserService,
            ProductEnum::S2LOW,
            1,
            [
                'authority_group_id' => 1,
                'department' => 1,
                'district' => 1,
                'authority_type_id' => 1,
                'service_name' => 'Root',
            ]
        );
        $this->assertTrue(
            $entiteProvisioning->add(simplexml_load_file(__DIR__ . '/../../fixtures/organism.xml'))
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testUpdate()
    {
        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var S2lowClient|MockObject $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $s2lowClient
            ->expects($this->once())
            ->method('getAuthority')
            ->with(123)
            ->willReturn(S2lowAuthority::hydrate([
                'id' => '123',
                'name' => 'Ville',
                'siren' => '000000000',
                'authority_group_id' => '1',
                'email' => 'email@example.org',
                'status' => '1',
                'authority_type_id' => '55',
                'address' => 'address',
                'postal_code' => '34000',
                'city' => 'Montpellier',
                'department' => '034',
                'district' => '3',
                'module' => [
                    1 => '1',
                    2 => '',
                    4 => '',
                ],
            ]));
        $s2lowClient
            ->expects($this->once())
            ->method('createOrUpdateAuthority')
            ->willReturn([
                'id' => 123
            ]);

        $s2lowTechnicalUserService = $this->createMock(S2lowTechnicalUserService::class);

        $entiteProvisioning = new EntiteProvisioning(
            $productOrganizationSocleRepository,
            $s2lowClient,
            $s2lowTechnicalUserService,
            ProductEnum::S2LOW,
            1,
            [
                'authority_group_id' => 1,
                'department' => 1,
                'district' => 1,
                'authority_type_id' => 1,
                'service_name' => 'Root',
            ]
        );
        $this->assertTrue(
            $entiteProvisioning->update(simplexml_load_file(__DIR__ . '/../../fixtures/organism.xml'), 123)
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testDelete()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            ProductEnum::S2LOW,
            1,
            '100000005',
            123
        );
        $authority = S2lowAuthority::hydrate(
            [
                'id' => '123',
                'name' => 'Ville',
                'siren' => '000000000',
                'authority_group_id' => '1',
                'email' => 'email@example.org',
                'status' => '1',
                'authority_type_id' => '55',
                'address' => 'address',
                'postal_code' => '34000',
                'city' => 'Montpellier',
                'department' => '034',
                'district' => '3',
                'module' => [
                    1 => '1',
                    2 => '',
                    4 => '',
                ],
            ]
        );

        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('getByOrganizationProductId')
            ->with(ProductEnum::S2LOW, 1, 123)
            ->willReturn($productOrganizationSocle);
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('delete')
            ->willReturn(true);

        /** @var MockObject|S2lowClient $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $s2lowClient
            ->expects($this->once())
            ->method('getAuthority')
            ->with(123)
            ->willReturn($authority);

        $authority->status = 0;

        $s2lowClient
            ->expects($this->once())
            ->method('createOrUpdateAuthority')
            ->with($authority)
            ->willReturn([]);

        $s2lowTechnicalUserService = $this->createMock(S2lowTechnicalUserService::class);

        $entiteProvisioning = new EntiteProvisioning(
            $productOrganizationSocleRepository,
            $s2lowClient,
            $s2lowTechnicalUserService,
            ProductEnum::S2LOW,
            1,
            [
                'authority_group_id' => 1,
                'department' => 1,
                'district' => 1,
                'authority_type_id' => 1,
            ]);
        $this->assertTrue($entiteProvisioning->delete(123));
    }

    /**
     * @throws ProductOrganizationSocleNotFoundException
     */
    public function testGetId()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            ProductEnum::S2LOW,
            1,
            '100000005',
            123
        );
        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('get')
            ->with(ProductEnum::S2LOW, 1, 7)
            ->willReturn($productOrganizationSocle);

        /** @var MockObject|S2lowClient $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $s2lowTechnicalUserService = $this->createMock(S2lowTechnicalUserService::class);

        $provisioningUser = new EntiteProvisioning(
            $productOrganizationSocleRepository,
            $s2lowClient,
            $s2lowTechnicalUserService,
            ProductEnum::S2LOW,
            1,
            []
        );
        $this->assertEquals(123, $provisioningUser->getId(7));
    }
}
