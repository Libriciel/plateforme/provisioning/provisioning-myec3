<?php

namespace App\Tests\Product\Parapheur;

use App\Entity\ProductUserSocle;
use App\Enum\ProductEnum;
use App\Repository\ProductUserSocleRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use App\Exception\ProductUserSocleNotFoundException;
use App\Http\Client\ParapheurClient;
use App\Http\Client\ParapheurClientException;
use App\Product\Parapheur\UserProvisioning;
use Psr\Http\Client\ClientExceptionInterface;

class UserProvisioningTest extends TestCase
{

    private function getProductUserSocle(
        string $product,
        int $number,
        string $socleId,
        string $productId
    ): ProductUserSocle {
        $productUserSocle = new ProductUserSocle();
        $productUserSocle->setProductId($product);
        $productUserSocle->setProductNumber($number);
        $productUserSocle->setUserSocleId($socleId);
        $productUserSocle->setUserProductId($productId);
        return $productUserSocle;
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testAdd()
    {
        $productUserSocle = $this->getProductUserSocle(ProductEnum::PARAPHEUR_MONOTENANT, 1, '300005473', 123);

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('add')
            ->with($productUserSocle)
            ->willReturn(true);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $parapheurClient
            ->expects($this->once())
            ->method('createUser')
            ->willReturn([
                'id' => 123,
            ]);

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MONOTENANT,
            1
        );
        $this->assertTrue(
            $provisioningUser->add(
                simplexml_load_file(__DIR__ . '/../../fixtures/agent.xml'),
                0,
                ""
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function testUpdate()
    {
        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();


        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $parapheurClient
            ->expects($this->once())
            ->method('updateUser');

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MONOTENANT,
            1
        );
        $this->assertTrue(
            $provisioningUser->update(simplexml_load_file(__DIR__ . '/../../fixtures/agent.xml'), 123, 111, "")
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function testDelete()
    {
        $productUserSocle = $this->getProductUserSocle(ProductEnum::PARAPHEUR_MONOTENANT, 1, '300005473', 123);

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('getByUserProductId')
            ->with(ProductEnum::PARAPHEUR_MONOTENANT, 1, 123)
            ->willReturn($productUserSocle);
        $productUserSocleRepository
            ->expects($this->once())
            ->method('delete')
            ->willReturn(true);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $parapheurClient
            ->expects($this->once())
            ->method('deleteUser');

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MONOTENANT,
            1
        );
        $this->assertTrue($provisioningUser->delete(123));
    }

    /**
     * @throws ProductUserSocleNotFoundException
     */
    public function testGetId()
    {
        $productUserSocle = $this->getProductUserSocle(ProductEnum::PARAPHEUR_MONOTENANT, 1, 7, 123);

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $productUserSocleRepository
            ->expects($this->once())
            ->method('get')
            ->with(ProductEnum::PARAPHEUR_MONOTENANT, 1, 7)
            ->willReturn($productUserSocle);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MONOTENANT,
            1
        );
        $this->assertEquals(123, $provisioningUser->getId(7));
    }
}
