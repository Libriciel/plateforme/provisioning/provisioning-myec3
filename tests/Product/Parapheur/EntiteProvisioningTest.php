<?php

namespace App\Tests\Product\Parapheur;

use App\Entity\ProductOrganizationSocle;
use App\Enum\ProductEnum;
use App\Http\Client\ParapheurClient;
use App\Http\Client\ParapheurClientException;
use App\Product\Parapheur\EntiteProvisioning;
use App\Repository\ProductOrganizationSocleRepository;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class EntiteProvisioningTest extends TestCase
{

    private function getProductOrganizationSocle(
        string $product,
        int $number,
        string $socleId,
        string $productId
    ): ProductOrganizationSocle {
        $productOrganizationSocle = new ProductOrganizationSocle();
        $productOrganizationSocle->setProductId($product);
        $productOrganizationSocle->setProductNumber($number);
        $productOrganizationSocle->setOrganizationSocleId($socleId);
        $productOrganizationSocle->setOrganizationProductId($productId);
        return $productOrganizationSocle;
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testAdd()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            ProductEnum::PARAPHEUR_MONOTENANT,
            1,
            '5',
            123
        );

        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('add')
            ->with($productOrganizationSocle)
            ->willReturn(true);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $parapheurClient
            ->expects($this->once())
            ->method('createGroup')
            ->willReturn([
                'id' => 123,
            ]);

        $entiteProvisioning = new EntiteProvisioning(
            $productOrganizationSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MONOTENANT,
            1
        );
        $this->assertTrue(
            $entiteProvisioning->add(
                simplexml_load_string(file_get_contents(__DIR__ . '/../../fixtures/organism.xml'))
            )
        );
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testDelete()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            ProductEnum::PARAPHEUR_MONOTENANT,
            1,
            '100000005',
            123
        );

        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('getByOrganizationProductId')
            ->with(ProductEnum::PARAPHEUR_MONOTENANT, 1, 123)
            ->willReturn($productOrganizationSocle);
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('delete')
            ->with($productOrganizationSocle)
            ->willReturn(true);


        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();
        $parapheurClient
            ->expects($this->once())
            ->method('deleteGroup')
            ->willReturn(true);

        $entiteProvisioning = new EntiteProvisioning(
            $productOrganizationSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MONOTENANT,
            1
        );

        $this->assertTrue($entiteProvisioning->delete(123));
    }

    public function testGetId()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            ProductEnum::PARAPHEUR_MONOTENANT,
            1,
            '100000005',
            123
        );
        /** @var ProductOrganizationSocleRepository|MockObject $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('get')
            ->with(ProductEnum::PARAPHEUR_MONOTENANT, 1, 7)
            ->willReturn($productOrganizationSocle);

        /** @var MockObject|ParapheurClient $parapheurClient */
        $parapheurClient = $this->getMockBuilder(ParapheurClient::class)
            ->disableOriginalConstructor()
            ->getMock();


        $provisioningUser = new EntiteProvisioning(
            $productOrganizationSocleRepository,
            $parapheurClient,
            ProductEnum::PARAPHEUR_MONOTENANT,
            1
        );
        $this->assertEquals(123, $provisioningUser->getId(7));
    }
}
