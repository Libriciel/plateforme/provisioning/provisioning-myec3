<?php

namespace App\Tests\Product\Pastell;

use App\Entity\ProductUserSocle;
use App\Enum\ProductEnum;
use App\Exception\ProductUserSocleNotFoundException;
use App\Product\Pastell\UserProvisioning;
use App\Repository\ProductUserSocleRepository;
use PastellClient\Api\UserRolesRequester;
use PastellClient\Api\UsersRequester;
use PastellClient\Exception\PastellException;
use PastellClient\Hydrator\UserHydrator;
use PastellClient\Model\UserRole;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class UserProvisioningTest extends TestCase
{

    /**
     * @var UserHydrator
     */
    private $userHydrator;

    protected function setUp(): void
    {
        $this->userHydrator = new UserHydrator();
    }

    private function getProductUserSocle(
        string $product,
        int $number,
        string $socleId,
        string $productId
    ): ProductUserSocle {
        $productOrganizationSocle = new ProductUserSocle();
        $productOrganizationSocle->setProductId($product);
        $productOrganizationSocle->setProductNumber($number);
        $productOrganizationSocle->setUserSocleId($socleId);
        $productOrganizationSocle->setUserProductId($productId);
        return $productOrganizationSocle;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws PastellException
     */
    public function testAdd()
    {
        $productUserSocle = $this->getProductUserSocle(
            ProductEnum::PASTELL,
            1,
            '300005473',
            123
        );

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('add')
            ->with($productUserSocle)
            ->willReturn(true);

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UsersRequester $usersRequesterMock */
        $usersRequesterMock = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $usersRequesterMock
            ->expects($this->once())
            ->method('create')
            ->willReturnCallback(function ($arg) {
                $this->assertEquals(12, $arg->id_e);
                return $this->userHydrator->hydrate([
                    'id_u' => 123,
                    'login' => 'klauss_heissler',
                    'prenom' => 'Klaus',
                    'nom' => 'Heissler',
                    'email' => 'klausheissler@gmail.com',
                ]);
            });

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $userRolesRequester,
            $usersRequesterMock,
            ProductEnum::PASTELL,
            1
        );
        $this->assertTrue(
            $provisioningUser->add(
                simplexml_load_file(__DIR__ . '/../../fixtures/agent.xml'),
                12,
                ""
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws PastellException
     */
    public function testAddDuplicatedPastellLogin()
    {
        $productUserSocle = $this->getProductUserSocle(
            ProductEnum::PASTELL,
            1,
            '300005473',
            123
        );

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('add')
            ->with($productUserSocle)
            ->willReturn(true);

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UsersRequester $usersRequesterMock */
        $usersRequesterMock = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $usersRequesterMock
            ->expects($this->once())
            ->method('create')
            ->willThrowException(new PastellException(json_encode(['error-message' => 'Ce login existe déjà'])));
        $usersRequesterMock
            ->expects($this->once())
            ->method('all')
            ->willReturn([
                $this->userHydrator->hydrate([
                    'login' => 'klauss_heissler',
                    'id_u' => 123,
                    'email' => 'mail'
                ])
            ]);

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $userRolesRequester,
            $usersRequesterMock,
            ProductEnum::PASTELL,
            1
        );
        $this->assertTrue(
            $provisioningUser->add(
                simplexml_load_file(__DIR__ . '/../../fixtures/agent.xml'),
                12,
                ""
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testUpdate()
    {
        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userRolesRequester
            ->expects($this->once())
            ->method('all')
            ->willReturn([]);

        /** @var MockObject|UsersRequester $usersRequesterMock */
        $usersRequesterMock = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $usersRequesterMock
            ->expects($this->once())
            ->method('show')
            ->with(123)
            ->willReturn($this->userHydrator->hydrate([
                'id_u' => 123,
                'login' => 'klauss_heissler',
                'prenom' => 'Klaus',
                'nom' => 'Heissler',
                'email' => 'klausheissler@gmail.com',
            ]));

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $userRolesRequester,
            $usersRequesterMock,
            ProductEnum::PASTELL,
            1
        );
        $this->assertTrue(
            $provisioningUser->update(
                simplexml_load_string(file_get_contents(__DIR__ . '/../../fixtures/agent.xml')),
                123,
                123,
                ""
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testUpdateWithOrganismChange()
    {
        $userRoleOriginal = new UserRole();
        $userRoleOriginal->id_u = 123;
        $userRoleOriginal->role = 'role name';
        $userRoleOriginal->id_e = 1;

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $userRolesRequester
            ->expects($this->once())
            ->method('all')
            ->willReturn([$userRoleOriginal]);
        $userRolesRequester
            ->expects($this->once())
            ->method('remove')
            ->with($userRoleOriginal);

        $newUserRole = new UserRole();
        $newUserRole->id_u = 123;
        $newUserRole->role = 'role name';
        $newUserRole->id_e = 2;
        $userRolesRequester
            ->expects($this->once())
            ->method('add')
            ->with($newUserRole);

        $userRolesRequester
            ->expects($this->once())
            ->method('remove')
            ->with($userRoleOriginal);

        /** @var MockObject|UsersRequester $usersRequesterMock */
        $usersRequesterMock = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $usersRequesterMock
            ->expects($this->once())
            ->method('show')
            ->with(123)
            ->willReturn($this->userHydrator->hydrate([
                'id_u' => 123,
                'login' => 'klauss_heissler',
                'prenom' => 'Klaus',
                'nom' => 'Heissler',
                'email' => 'klausheissler@gmail.com',
                'id_e' => 1
            ]));

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $userRolesRequester,
            $usersRequesterMock,
            ProductEnum::PASTELL,
            1
        );
        $this->assertTrue(
            $provisioningUser->update(
                simplexml_load_string(file_get_contents(__DIR__ . '/../../fixtures/agent.xml')),
                123,
                2,
                "",
                '2'
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testDelete()
    {
        $productUserSocle = $this->getProductUserSocle(
            ProductEnum::PASTELL,
            1,
            '300005473',
            123
        );
        $user = $this->userHydrator->hydrate([
            'id_u' => 123,
            'login' => 'klauss_heissler',
            'prenom' => 'Klaus',
            'nom' => 'Heissler',
            'email' => 'klausheissler@gmail.com',
        ]);

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('getByUserProductId')
            ->with(ProductEnum::PASTELL, 1, 123)
            ->willReturn($productUserSocle);
        $productUserSocleRepository
            ->expects($this->once())
            ->method('delete')
            ->willReturn(true);

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UsersRequester $usersRequesterMock */
        $usersRequesterMock = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $usersRequesterMock
            ->expects($this->once())
            ->method('remove')
            ->with(123);

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $userRolesRequester,
            $usersRequesterMock,
            ProductEnum::PASTELL,
            1
        );
        $this->assertTrue($provisioningUser->delete(123));
    }

    /**
     * @throws ProductUserSocleNotFoundException
     */
    public function testGetId()
    {
        $productUserSocle = $this->getProductUserSocle(
            ProductEnum::PASTELL,
            1,
            7,
            123
        );

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $productUserSocleRepository
            ->expects($this->once())
            ->method('get')
            ->with(ProductEnum::PASTELL, 1, 7)
            ->willReturn($productUserSocle);

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UsersRequester $usersRequester */
        $usersRequester = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $userRolesRequester,
            $usersRequester,
            ProductEnum::PASTELL,
            1
        );
        $this->assertEquals(123, $provisioningUser->getId(7));
    }

    /**
     * @throws ProductUserSocleNotFoundException
     */
    public function testGetNotExistingUserId()
    {

        /** @var MockObject|ProductUserSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductUserSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository
            ->expects($this->once())
            ->method('exists')
            ->willReturn(false);

        /** @var MockObject|UserRolesRequester $userRolesRequester */
        $userRolesRequester = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|UsersRequester $usersRequester */
        $usersRequester = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $provisioningUser = new UserProvisioning(
            $productUserSocleRepository,
            $userRolesRequester,
            $usersRequester,
            ProductEnum::PASTELL,
            1
        );
        $this->assertNull($provisioningUser->getId(7));
    }
}
