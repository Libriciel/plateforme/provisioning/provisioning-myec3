<?php

namespace App\Tests\Product\Pastell;

use App\Entity\ProductDepartmentSocle;
use App\Entity\ProductOrganizationSocle;
use App\Enum\ProductEnum;
use App\Product\Pastell\DepartmentProvisioning;
use App\Repository\ProductDepartmentSocleRepository;
use App\Repository\ProductOrganizationSocleRepository;
use App\Service\PastellConnectorsService;
use PastellClient\Api\EntitesRequester;
use PastellClient\Hydrator\EntiteHydrator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class DepartmentProvisioningTest extends TestCase
{
    /**
     * @var EntiteHydrator
     */
    private $entiteHydrator;

    protected function setUp(): void
    {
        $this->entiteHydrator = new EntiteHydrator();
    }

    private function getProductDepartmentSocle(
        string $product,
        int $number,
        string $socleId,
        string $productId
    ): ProductDepartmentSocle {
        $productDepartmentSocle = new ProductDepartmentSocle();
        $productDepartmentSocle->setProductId($product);
        $productDepartmentSocle->setProductNumber($number);
        $productDepartmentSocle->setDepartmentSocleId($socleId);
        $productDepartmentSocle->setDepartmentProductId($productId);
        return $productDepartmentSocle;
    }

    private function getProductOrganizationSocle(
        string $product,
        int $number,
        string $socleId,
        string $productId
    ): ProductOrganizationSocle {
        $productOrganizationSocle = new ProductOrganizationSocle();
        $productOrganizationSocle->setProductId($product);
        $productOrganizationSocle->setProductNumber($number);
        $productOrganizationSocle->setOrganizationSocleId($socleId);
        $productOrganizationSocle->setOrganizationProductId($productId);
        return $productOrganizationSocle;
    }

    public function testAddRootDepartment()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            ProductEnum::PASTELL,
            1,
            '100000005',
            123
        );
        $productDepartmentSocle = $this->getProductDepartmentSocle(
            ProductEnum::PASTELL,
            1,
            '1096',
            123
        );

        /** @var MockObject|ProductDepartmentSocleRepository $productDepartmentSocleRepository */
        $productDepartmentSocleRepository = $this
            ->getMockBuilder(ProductDepartmentSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productDepartmentSocleRepository
            ->expects($this->once())
            ->method('add')
            ->with($productDepartmentSocle)
            ->willReturn(true);

        /** @var MockObject|ProductOrganizationSocleRepository $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productOrganizationSocleRepository
            ->expects($this->once())
            ->method('get')
            ->with(ProductEnum::PASTELL, 1, '100000005')
            ->willReturn($productOrganizationSocle);

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();


        $pastellConnectorsServiceMock = $this->createMock(PastellConnectorsService::class);

        $departmentProvisioning = new DepartmentProvisioning(
            $productDepartmentSocleRepository,
            $productOrganizationSocleRepository,
            $entitesRequesterMock,
            $pastellConnectorsServiceMock,
            ProductEnum::PASTELL,
            1
        );
        $this->assertTrue(
            $departmentProvisioning->add(simplexml_load_string(file_get_contents(__DIR__ . '/../../fixtures/department.xml')))
        );
    }

    public function testUpdate()
    {
        $productDepartmentSocle = $this->getProductDepartmentSocle(
            ProductEnum::PASTELL,
            1,
            '1096',
            123
        );
        /** @var MockObject|ProductDepartmentSocleRepository $productDepartmentSocleRepository */
        $productDepartmentSocleRepository = $this
            ->getMockBuilder(ProductDepartmentSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $productDepartmentSocleRepository
            ->expects($this->once())
            ->method('get')
            ->willReturn($productDepartmentSocle);

        /** @var MockObject|ProductOrganizationSocleRepository $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $entitesRequesterMock
            ->expects($this->once())
            ->method('show')
            ->with(123)
            ->willReturn($this->entiteHydrator->hydrate([
                'id_e' => 123,
                'denomination' => 'name',
                'siren' => '000000000',
                'type' => 'collectivite',
                'entite_mere' => '0'
            ]));

        $pastellConnectorsServiceMock = $this->createMock(PastellConnectorsService::class);

        $departmentProvisioning = new DepartmentProvisioning(
            $productDepartmentSocleRepository,
            $productOrganizationSocleRepository,
            $entitesRequesterMock,
            $pastellConnectorsServiceMock,
            ProductEnum::PASTELL,
            1
        );
        $this->assertTrue(
            $departmentProvisioning->update(
                simplexml_load_string(file_get_contents(__DIR__ . '/../../fixtures/department.xml')),
                123
            )
        );
    }

    public function testDelete()
    {
        $productDepartmentSocle = $this->getProductDepartmentSocle(
            ProductEnum::PASTELL,
            1,
            '1096',
            123
        );
        /** @var MockObject|ProductDepartmentSocleRepository $productDepartmentSocleRepository */
        $productDepartmentSocleRepository = $this
            ->getMockBuilder(ProductDepartmentSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $productDepartmentSocleRepository
            ->expects($this->once())
            ->method('getByDepartmentProductId')
            ->with(ProductEnum::PASTELL, 1, 123)
            ->willReturn($productDepartmentSocle);
        $productDepartmentSocleRepository
            ->expects($this->once())
            ->method('delete')
            ->willReturn(true);

        /** @var MockObject|ProductOrganizationSocleRepository $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pastellConnectorsServiceMock = $this->createMock(PastellConnectorsService::class);

        $departmentProvisioning = new DepartmentProvisioning(
            $productDepartmentSocleRepository,
            $productOrganizationSocleRepository,
            $entitesRequesterMock,
            $pastellConnectorsServiceMock,
            ProductEnum::PASTELL,
            1
        );
        $this->assertTrue($departmentProvisioning->delete(123));
    }


    public function testGetId()
    {
        $productDepartmentSocle = $this->getProductDepartmentSocle(
            ProductEnum::PASTELL,
            1,
            '1096',
            123
        );
        /** @var MockObject|ProductDepartmentSocleRepository $productDepartmentSocleRepository */
        $productDepartmentSocleRepository = $this
            ->getMockBuilder(ProductDepartmentSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        $productDepartmentSocleRepository
            ->expects($this->once())
            ->method('exists')
            ->willReturn(true);

        $productDepartmentSocleRepository
            ->expects($this->once())
            ->method('get')
            ->with(ProductEnum::PASTELL, 1, '1096')
            ->willReturn($productDepartmentSocle);

        /** @var MockObject|ProductOrganizationSocleRepository $productOrganizationSocleRepository */
        $productOrganizationSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pastellConnectorsServiceMock = $this->createMock(PastellConnectorsService::class);

        $departmentProvisioning = new DepartmentProvisioning(
            $productDepartmentSocleRepository,
            $productOrganizationSocleRepository,
            $entitesRequesterMock,
            $pastellConnectorsServiceMock,
            ProductEnum::PASTELL,
            1
        );
        $this->assertEquals(123, $departmentProvisioning->getId('1096'));
    }
}
