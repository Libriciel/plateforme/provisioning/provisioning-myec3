<?php

namespace App\Tests\Product\Pastell;

use App\Entity\ProductOrganizationSocle;
use App\Enum\ProductEnum;
use App\Exception\ProductOrganizationSocleNotFoundException;
use App\Product\Pastell\EntiteProvisioning;
use App\Repository\ProductOrganizationSocleRepository;
use App\Service\PastellTechnicalUserService;
use PastellClient\Api\EntitesRequester;
use PastellClient\Hydrator\EntiteHydrator;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use Psr\Http\Client\ClientExceptionInterface;

class EntiteProvisioningTest extends TestCase
{

    /**
     * @var EntiteHydrator
     */
    private $entiteHydrator;

    protected function setUp(): void
    {
        $this->entiteHydrator = new EntiteHydrator();
    }

    private function getProductOrganizationSocle(
        string $product,
        int $number,
        string $socleId,
        string $productId
    ): ProductOrganizationSocle {
        $productOrganizationSocle = new ProductOrganizationSocle();
        $productOrganizationSocle->setProductId($product);
        $productOrganizationSocle->setProductNumber($number);
        $productOrganizationSocle->setOrganizationSocleId($socleId);
        $productOrganizationSocle->setOrganizationProductId($productId);
        return $productOrganizationSocle;
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testAdd()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(
            ProductEnum::PASTELL,
            1,
            '100000005',
            123
        );

        /** @var MockObject|ProductOrganizationSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('add')
            ->with($productOrganizationSocle)
            ->willReturn(true);

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $entitesRequesterMock->expects($this->once())
            ->method('create')
            ->willReturn($this->entiteHydrator->hydrate([
                'id_e' => 123,
                'denomination' => 'name',
                'siren' => '000000000',
                'type' => 'collectivite',
                'entite_mere' => '0'
            ]));

        $pastellTechnicalUserServiceMock = $this->createMock(PastellTechnicalUserService::class);

        $provisioningUser = new EntiteProvisioning(
            $productUserSocleRepository,
            $entitesRequesterMock,
            $pastellTechnicalUserServiceMock,
            ProductEnum::PASTELL,
            1
        );
        $this->assertTrue(
            $provisioningUser->add(simplexml_load_file(__DIR__ . '/../../fixtures/organism.xml'))
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testUpdate()
    {
        /** @var MockObject|ProductOrganizationSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $entitesRequesterMock->expects($this->once())
            ->method('show')
            ->with(123)
            ->willReturn($this->entiteHydrator->hydrate([
                'id_e' => 123,
                'denomination' => 'name',
                'siren' => '000000000',
                'type' => 'collectivite',
                'entite_mere' => '0'
            ]));

        $pastellTechnicalUserServiceMock = $this->createMock(PastellTechnicalUserService::class);

        $provisioningUser = new EntiteProvisioning(
            $productUserSocleRepository,
            $entitesRequesterMock,
            $pastellTechnicalUserServiceMock,
        ProductEnum::PASTELL,
            1
        );
        $this->assertTrue(
            $provisioningUser->update(simplexml_load_file(__DIR__ . '/../../fixtures/organism.xml'), 123)
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testDelete()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(ProductEnum::PASTELL, 1, 5, 123);

        /** @var MockObject|ProductOrganizationSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('getByOrganizationProductId')
            ->with(ProductEnum::PASTELL, 1, 123)
            ->willReturn($productOrganizationSocle);
        $productUserSocleRepository->expects($this->once())
            ->method('delete')
            ->willReturn(true);

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pastellTechnicalUserServiceMock = $this->createMock(PastellTechnicalUserService::class);

        $provisioningUser = new EntiteProvisioning(
            $productUserSocleRepository,
            $entitesRequesterMock,
            $pastellTechnicalUserServiceMock,
        ProductEnum::PASTELL,
            1
        );
        $this->assertTrue($provisioningUser->delete(123));
    }


    /**
     * @throws ProductOrganizationSocleNotFoundException
     */
    public function testGetId()
    {
        $productOrganizationSocle = $this->getProductOrganizationSocle(ProductEnum::PASTELL, 1, 5, 123);

        /** @var MockObject|ProductOrganizationSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('exists')
            ->willReturn(true);
        $productUserSocleRepository->expects($this->once())
            ->method('get')
            ->with(ProductEnum::PASTELL, 1, 5)
            ->willReturn($productOrganizationSocle);

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pastellTechnicalUserServiceMock = $this->createMock(PastellTechnicalUserService::class);

        $provisioningUser = new EntiteProvisioning(
            $productUserSocleRepository,
            $entitesRequesterMock,
            $pastellTechnicalUserServiceMock,
        ProductEnum::PASTELL,
            1
        );
        $this->assertEquals(123, $provisioningUser->getId(5));
    }

    /**
     * @throws ProductOrganizationSocleNotFoundException
     */
    public function testGetNotExistingOrganismId()
    {
        /** @var MockObject|ProductOrganizationSocleRepository $productUserSocleRepository */
        $productUserSocleRepository = $this
            ->getMockBuilder(ProductOrganizationSocleRepository::class)
            ->disableOriginalConstructor()
            ->getMock();
        $productUserSocleRepository->expects($this->once())
            ->method('exists')
            ->willReturn(false);

        /** @var MockObject|EntitesRequester $entitesRequesterMock */
        $entitesRequesterMock = $this->getMockBuilder(EntitesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pastellTechnicalUserServiceMock = $this->createMock(PastellTechnicalUserService::class);

        $provisioningUser = new EntiteProvisioning(
            $productUserSocleRepository,
            $entitesRequesterMock,
            $pastellTechnicalUserServiceMock,
        ProductEnum::PASTELL,
            1
        );
        $this->assertNull($provisioningUser->getId(5));
    }
}
