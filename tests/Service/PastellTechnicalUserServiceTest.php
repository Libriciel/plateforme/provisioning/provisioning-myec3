<?php

namespace App\Tests\Service;

use App\Service\HashService;
use App\Service\PastellTechnicalUserService;
use PastellClient\Api\UserRolesRequester;
use PastellClient\Api\UsersRequester;
use PastellClient\Exception\PastellException;
use PastellClient\Hydrator\UserHydrator;
use PastellClient\Model\UserRole;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class PastellTechnicalUserServiceTest extends TestCase
{
    /**
     * @var UserHydrator
     */
    private $userHydrator;

    protected function setUp(): void
    {
        $this->userHydrator = new UserHydrator();
    }

    public function testCreate()
    {
        /** @var MockObject|UsersRequester $usersRequesterMock */
        $usersRequesterMock = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $usersRequesterMock
            ->expects($this->once())
            ->method('create')
            ->willReturnCallback(function ($arg) {
                $this->assertEquals(1, $arg->id_e);
                return $this->userHydrator->hydrate([
                    'id_u' => 123,
                    'login' => 'tech_1096',
                    'prenom' => 'User',
                    'nom' => 'Tech',
                    'email' => 'email@example.org',
                ]);
            });

        /** @var MockObject|UserRolesRequester $userRolesRequesterMock */
        $userRolesRequesterMock = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $userRole = new UserRole();
        $userRole->id_u = 123;
        $userRole->id_e = 1;
        $userRole->role = 'role1';
        $userRolesRequesterMock
            ->expects($this->at(0))
            ->method('add')
            ->with($userRole);

        $pastellTechnicalUserService = new PastellTechnicalUserService(
            $usersRequesterMock,
            $userRolesRequesterMock,
            new HashService('SECRET_KEY'),
            'email@example.org',
            ['role1', 'role2']
        );

        $pastellTechnicalUserService->create(
            simplexml_load_string(
                file_get_contents(__DIR__ . '/../fixtures/department.xml')
            ),
            1
        );
    }

    public function testCreateWithExistingTechUser()
    {
        /** @var MockObject|UsersRequester $usersRequesterMock */
        $usersRequesterMock = $this->getMockBuilder(UsersRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $usersRequesterMock
            ->expects($this->once())
            ->method('create')
            ->willThrowException(new PastellException(json_encode(['error-message' => 'Ce login existe déjà'])));
        $usersRequesterMock
            ->expects($this->once())
            ->method('all')
            ->willReturn([
                $this->userHydrator->hydrate([
                    'login' => 'tech_1096',
                    'id_u' => 123,
                    'email' => 'email@example.org'
                ])
            ]);

        /** @var MockObject|UserRolesRequester $userRolesRequesterMock */
        $userRolesRequesterMock = $this->getMockBuilder(UserRolesRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $userRole = new UserRole();
        $userRole->id_u = 123;
        $userRole->id_e = 1;
        $userRole->role = 'role1';
        $userRolesRequesterMock
            ->expects($this->at(0))
            ->method('add')
            ->with($userRole);

        $pastellTechnicalUserService = new PastellTechnicalUserService(
            $usersRequesterMock,
            $userRolesRequesterMock,
            new HashService('SECRET_KEY'),
            'email@example.org',
            ['role1', 'role2']
        );

        $pastellTechnicalUserService->create(
            simplexml_load_string(
                file_get_contents(__DIR__ . '/../fixtures/department.xml')
            ),
            1
        );
    }
}
