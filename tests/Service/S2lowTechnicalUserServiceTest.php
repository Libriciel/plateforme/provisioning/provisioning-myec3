<?php

namespace App\Tests\Service;

use App\Http\Client\S2lowClient;
use App\Service\HashService;
use App\Service\S2lowTechnicalUserService;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class S2lowTechnicalUserServiceTest extends TestCase
{

    public function testCreate()
    {
        /** @var MockObject|S2lowClient $s2lowClient */
        $s2lowClient = $this->getMockBuilder(S2lowClient::class)
            ->disableOriginalConstructor()
            ->getMock();

        $s2lowClient
            ->expects($this->once())
            ->method('createOrUpdateUser')
            ->willReturn([
                'id' => 123,
            ]);
        $s2lowClient
            ->expects($this->once())
            ->method('getAuthorityServices')
            ->willReturn([
                [
                    'id' => 321
                ]
            ]);
        $s2lowClient
            ->expects($this->once())
            ->method('addUserToService');

        $s2lowTechnicalUserService = new S2lowTechnicalUserService(
            $s2lowClient,
            new HashService('SECRET_KEY'),
            [
                'email' => 'tech@example.org',
                'certificate' => 'CERTIFICATE CONTENT',
                'authority_group_id' => 1,
                'role' => 'role',
            ]
        );

        $s2lowTechnicalUserService->create('123',1);
    }
}
