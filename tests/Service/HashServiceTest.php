<?php

namespace App\Tests\Service;

use App\Service\HashService;
use PHPUnit\Framework\TestCase;

class HashServiceTest extends TestCase
{

    public function algoProvider(): iterable
    {
        yield ['md5', 'md5'];
        yield ['sha512', 'sha512'];
        yield ['', HashService::DEFAULT_ALGO];
        yield ['unknown', HashService::DEFAULT_ALGO];
    }

    /**
     * @dataProvider algoProvider
     */
    public function testSetAlgo(string $algo, string $expected): void
    {
        $hashService = new HashService('KEY');
        $hashService->setAlgo($algo);

        $this->assertSame($expected, $hashService->getAlgo());
    }
}
