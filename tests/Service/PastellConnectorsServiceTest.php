<?php

namespace App\Tests\Service;

use App\Service\HashService;
use App\Service\PastellConnectorsService;
use PastellClient\Api\ConnectorsRequester;
use PastellClient\Api\ModuleAssociationsRequester;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;

class PastellConnectorsServiceTest extends TestCase
{
    private function getDefaultConnectorData(): array
    {
        return [
            PastellConnectorsService::LIBERSIGN => [
                'label' => 'libersign',
                'libersign_applet_url' => 'https://applet_libersign',
                'libersign_extension_update_url' => 'https://applet_libersign',
                'libersign_help_url' => 'https://help_libersign',
                'libersign_xmlstarlet_path' => '/usr/local/bin/xmlstarlet',
            ],
            PastellConnectorsService::S2LOW => [
                'label' => 's2low',
                'url' => 'https://s2low',
                'user_certificat' => [
                    'filepath' => '/app/data/cert.p12',
                    'password' => 'password',
                ],
                'authentication_for_teletransmisson' => true,
                'associations' => json_decode('["actes-generique","actes-automatique"]', true),
            ]
        ];
    }

    public function testSaveTdtConnector()
    {
        $defaultData = $this->getDefaultConnectorData();

        /** @var MockObject|ConnectorsRequester $connectorsRequesterMock */
        $connectorsRequesterMock = $this->getMockBuilder(ConnectorsRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $connectorsRequesterMock
            ->expects($this->once())
            ->method('all')
            ->willReturn([]);

        $connectorsRequesterMock
            ->expects($this->once())
            ->method('create')
            ->with(11, PastellConnectorsService::S2LOW, $defaultData[PastellConnectorsService::S2LOW]['label']);

        /** @var MockObject|ModuleAssociationsRequester $moduleAssociationsRequesterMock */
        $moduleAssociationsRequesterMock = $this->getMockBuilder(ModuleAssociationsRequester::class)
            ->disableOriginalConstructor()
            ->getMock();
        $moduleAssociationsRequesterMock
            ->expects($this->at(1))
            ->method('associate')
            ->with(
                $this->anything(),
                $this->anything(),
                'actes-generique',
                $this->anything()
            );

        $pastellConnectorsService = new PastellConnectorsService(
            $connectorsRequesterMock,
            $moduleAssociationsRequesterMock,
            new HashService('SECRET'),
            $defaultData
        );

        $pastellConnectorsService->saveTdTConnector(11, '1234');
    }

    public function testSaveLibersignConnector()
    {
        $defaultData = $this->getDefaultConnectorData();

        /** @var MockObject|ConnectorsRequester $connectorsRequesterMock */
        $connectorsRequesterMock = $this->getMockBuilder(ConnectorsRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $connectorsRequesterMock
            ->expects($this->once())
            ->method('all')
            ->willReturn([]);

        $connectorsRequesterMock
            ->expects($this->once())
            ->method('create')
            ->with(11, PastellConnectorsService::LIBERSIGN, $defaultData[PastellConnectorsService::LIBERSIGN]['label']);

        /** @var MockObject|ModuleAssociationsRequester $moduleAssociationsRequesterMock */
        $moduleAssociationsRequesterMock = $this->getMockBuilder(ModuleAssociationsRequester::class)
            ->disableOriginalConstructor()
            ->getMock();

        $pastellConnectorsService = new PastellConnectorsService(
            $connectorsRequesterMock,
            $moduleAssociationsRequesterMock,
            new HashService('SECRET'),
            $defaultData
        );

        $pastellConnectorsService->saveLibersignConnector(11);
    }
}
