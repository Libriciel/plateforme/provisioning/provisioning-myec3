<?php

namespace App\Tests\Http\Model;

use App\Http\Model\S2lowUser;
use PHPUnit\Framework\TestCase;

class S2lowUserTest extends TestCase
{

    public function testGetMultipartArray()
    {
        $user = new S2lowUser();
        $user->name = 'name';
        $user->givenname = 'given name';
        $user->email = 'email';
        $user->certificate = 'certificate';

        $this->assertSame(
            [
                [
                    'name' => 'api',
                    'contents' => 1,
                ],
                [
                    'name' => 'name',
                    'contents' => 'name',
                ],
                [
                    'name' => 'givenname',
                    'contents' => 'given name',
                ],
                [
                    'name' => 'email',
                    'contents' => 'email',
                ],
                [
                    'name' => 'certificate',
                    'contents' => 'certificate',
                    'filename' => 'certificate.pem',
                ],
            ],
            $user->getMultipartArray()
        );
    }
}
