<?php

namespace App\Tests\Http\Client;

use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\MockObject\MockObject;
use App\Http\Client\ParapheurClient;
use PHPUnit\Framework\TestCase;
use App\Http\Client\ParapheurClientException;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;

class ParapheurClientTest extends TestCase
{
    /**
     * @var ClientInterface|MockObject
     */
    private $httpClient;

    /**
     * @var ParapheurClient
     */
    private $parapheurClient;

    public function setUp(): void
    {
        $this->httpClient = $this->getMockBuilder(ClientInterface::class)->getMock();
        $this->parapheurClient = new ParapheurClient($this->httpClient);
    }

    public function tearDown(): void
    {
        $this->httpClient = null;
        $this->parapheurClient = null;
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testGetAccessTicket()
    {
        $ticket = '1234';
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'data' => ['ticket' => $ticket]
                    ])
                )
            );
        $this->assertSame($ticket, $this->parapheurClient->getAccessTicket());
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function testCreateUser()
    {
        $this->httpClient->expects($this->at(0))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'data' => ['ticket' => '1234']
                    ])
                )
            );
        $this->httpClient->expects($this->at(1))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'id' => '980a510c-881b-4728-a9bb-1a0588d904b3',
                    ])
                )
            );

        $this->assertSame(
            [
                'id' => '980a510c-881b-4728-a9bb-1a0588d904b3',
            ],
            $this->parapheurClient->createUser(
                'username',
                'firstname',
                'lastname',
                'email',
                'password'
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testCreateUserWithWrongId()
    {
        $this->httpClient->expects($this->at(0))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'data' => ['ticket' => '1234']
                    ])
                )
            );
        $this->httpClient->expects($this->at(1))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'id' => '123',
                    ])
                )
            );

        $this->expectException(ParapheurClientException::class);
        $this->expectExceptionMessage('{"id":"123"}');

        $this->parapheurClient->createUser(
            'username',
            'firstname',
            'lastname',
            'email',
            'password'
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function testUpdateUser()
    {
        $user = [
            'id' => '123',
            'firstname' => 'firstname2',
            'lastname' => 'lastname2',
            'email' => 'email2',
        ];

        $this->httpClient->expects($this->at(0))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'data' => ['ticket' => '1234']
                    ])
                )
            );
        $this->httpClient->expects($this->at(1))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    ''
                )
            );

        $this->assertTrue(
            $this->parapheurClient->updateUser(
                '123',
                'firstname2',
                'lastname2',
                'email2'
            )
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws ParapheurClientException
     */
    public function testDeleteUser()
    {
        $this->httpClient->expects($this->at(0))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'data' => ['ticket' => '1234']
                    ])
                )
            );
        $this->httpClient->expects($this->at(1))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([])
                )
            );

        $this->assertSame(
            [],
            $this->parapheurClient->deleteUser('123')
        );
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testGetGroupByName(): void
    {
        $this->httpClient
            ->expects($this->at(0))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'data' => ['ticket' => '1234']
                    ])
                )
            );
        $this->httpClient
            ->expects($this->at(1))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        [
                            'shortName' => 'groupName',
                            'id' => '123',
                        ]
                    ])
                )
            );

        $this->assertSame(
            '123',
            $this->parapheurClient->getGroupByName('groupName')
        );
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testCreateGroup(): void
    {
        $this->httpClient
            ->expects($this->at(0))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'data' => ['ticket' => '1234']
                    ])
                )
            );
        $this->httpClient
            ->expects($this->at(1))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'id' => '980a510c-881b-4728-a9bb-1a0588d904b3',
                    ])
                )
            );

        $this->assertSame(
            [
                'id' => '980a510c-881b-4728-a9bb-1a0588d904b3',
            ],
            $this->parapheurClient->createGroup('groupName')
        );
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testAddUserToGroup(): void
    {
        $this->httpClient
            ->expects($this->at(0))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'data' => ['ticket' => '1234']
                    ])
                )
            );
        $this->httpClient
            ->expects($this->at(1))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([])
                )
            );

        $this->assertSame(
            [],
            $this->parapheurClient->addUserToGroup('123', 'username')
        );
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testDeleteGroup(): void
    {
        $this->httpClient
            ->expects($this->at(0))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'data' => ['ticket' => '1234']
                    ])
                )
            );
        $this->httpClient
            ->expects($this->at(1))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([])
                )
            );

        $this->assertTrue(
            $this->parapheurClient->deleteGroup('123')
        );
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testGetTenants(): void
    {
        $this->httpClient
            ->expects($this->at(0))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'data' => ['ticket' => '1234']
                    ])
                )
            );
        $this->httpClient
            ->expects($this->at(1))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        [
                            "tenantDomain" => "tenant.test",
                            "title" => "tenant.test",
                            "enabled" => true
                        ]
                    ])
                )
            );

        $this->assertSame(
            [
                [
                    "tenantDomain" => "tenant.test",
                    "title" => "tenant.test",
                    "enabled" => true
                ],

            ],
            $this->parapheurClient->getTenants()
        );
    }


    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testCreateTenant(): void
    {
        $this->httpClient
            ->expects($this->at(0))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'data' => ['ticket' => '1234']
                    ])
                )
            );
        $this->httpClient
            ->expects($this->at(1))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([])
                )
            );
        $this->httpClient
            ->expects($this->at(2))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        [
                            'tenantDomain' => 'tenant.name'
                        ]
                    ])
                )
            );

        $this->assertTrue(
            $this->parapheurClient->createTenant(
                'tenant.name',
                'tenant.name',
                'tenant.name',
                'adminPassword'
            )
        );
    }

    /**
     * @throws ParapheurClientException
     * @throws ClientExceptionInterface
     */
    public function testToggleTenant(): void
    {
        $this->httpClient
            ->expects($this->at(0))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'data' => ['ticket' => '1234']
                    ])
                )
            );
        $this->httpClient
            ->expects($this->at(1))
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([])
                )
            );

        $this->assertTrue(
            $this->parapheurClient->toggleTenant('tenant.test', true)
        );
    }
}
