<?php

namespace App\Tests\Http\Client;

use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\MockObject\MockObject;
use PHPUnit\Framework\TestCase;
use App\Http\Client\S2lowClient;
use App\Http\Client\S2lowClientException;
use App\Http\Model\S2lowAuthority;
use App\Http\Model\S2lowUser;
use Psr\Http\Client\ClientExceptionInterface;
use Psr\Http\Client\ClientInterface;
use function json_encode;

class S2lowClientTest extends TestCase
{

    /**
     * @var ClientInterface|MockObject
     */
    private $httpClient;

    /**
     * @var S2lowClient
     */
    private $s2lowClient;

    public function setUp(): void
    {
        $this->httpClient = $this->getMockBuilder(ClientInterface::class)->getMock();
        $this->s2lowClient = new S2lowClient($this->httpClient);
    }

    public function tearDown(): void
    {
        $this->httpClient = null;
        $this->s2lowClient = null;
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testTestAuth()
    {
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    'OK'
                )
            );
        $this->assertSame('OK', $this->s2lowClient->testAuth());
    }

    /**
     * @throws S2lowClientException
     * @throws ClientExceptionInterface
     */
    public function testCreateOrUpdateUser()
    {
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'status' => 'ok',
                        'id' => 123
                    ])
                )
            );
        $this->assertSame(
            [
                'status' => 'ok',
                'id' => 123,
            ],
            $this->s2lowClient->createOrUpdateUser(new S2lowUser())
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testCreateOrUpdateUserException()
    {
        $this->expectException(S2lowClientException::class);
        $this->expectExceptionMessage('{"status":"error","error-message":"Impossible de lire le certificat"}');
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    400,
                    [],
                    json_encode([
                        'status' => 'error',
                        'error-message' => 'Impossible de lire le certificat'
                    ])
                )
            );
        $this->s2lowClient->createOrUpdateUser(new S2lowUser());
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testGetUser()
    {
        $this->httpClient
            ->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'name' => 'Klaus',
                        'givenname' => 'Heissler',
                        'login' => 'socle_10',
                        'email' => 'klausheissler@gmail.com',
                        'status' => 1,
                        'authority_id' => 1,
                        'authority_group_id' => 1,
                        'role' => 'role',
                        'certificate' => '...',
                    ])
                )
            );
        $this->assertEquals(
            S2lowUser::hydrate([
                'name' => 'Klaus',
                'givenname' => 'Heissler',
                'login' => 'socle_10',
                'email' => 'klausheissler@gmail.com',
                'authority_id' => 1,
                'status' => 1,
                'role' => 'role',
                'authority_group_id' => 1,
                'certificate' => '...',
            ], 123),
            $this->s2lowClient->getUser(123)
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testCreateAuthority()
    {
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'status' => 'ok',
                        'message' => 'Création de la collectivité (id=11). Résultat ok.',
                        'id' => '11',
                    ])
                )
            );
        $this->assertEquals(
            [
                'status' => 'ok',
                'message' => 'Création de la collectivité (id=11). Résultat ok.',
                'id' => '11',
            ],
            $this->s2lowClient->createOrUpdateAuthority(new S2lowAuthority())
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testGetAuthority()
    {
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        'id' => '123',
                        'name' => 'Ville',
                        'siren' => '000000000',
                        'authority_type_id' => '55',
                        'status' => '1',
                        'email' => 'email@example.org',
                        'default_broadcast_email' => '',
                        'broadcast_email' => '',
                        'address' => 'address',
                        'postal_code' => '34000',
                        'city' => 'Montpellier',
                        'department' => '034',
                        'district' => '3',
                        'telephone' => '',
                        'fax' => '',
                        'email_mail_securise' => '',
                        'pastell_url' => '',
                        'pastell_login' => '',
                        'pastell_password' => '',
                        'pastell_id_e' => '',
                        'authority_group_id' => '1',
                        'helios_ftp_dest' => '',
                        'module' => [
                            1 => '1',
                            2 => '1',
                            4 => '1',
                        ],
                    ])
                )
            );
        $this->assertEquals(
            S2lowAuthority::hydrate([
                'id' => '123',
                'name' => 'Ville',
                'siren' => '000000000',
                'authority_group_id' => '1',
                'email' => 'email@example.org',
                'status' => '1',
                'authority_type_id' => '55',
                'address' => 'address',
                'postal_code' => '34000',
                'city' => 'Montpellier',
                'department' => '034',
                'district' => '3',
                'module' => [
                    1 => '1',
                    2 => '1',
                    4 => '1',
                ],
            ]),
            $this->s2lowClient->getAuthority(123)
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testGetAuthorityServices()
    {
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([
                        [
                            'id' => '1',
                            'authority_id' => '123',
                            'name' => 'Root',
                            'parent_id' => '',
                        ],
                    ])
                )
            );
        $this->assertEquals(
            [
                [
                    'id' => '1',
                    'authority_id' => '123',
                    'name' => 'Root',
                    'parent_id' => '',
                ],
            ],
            $this->s2lowClient->getAuthorityServices(123)
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testCreateAuthorityService()
    {
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([

                        'status' => 'ok',
                        'message' => 'Le service a été créé',
                    ])
                )
            );
        $this->assertEquals(
            [
                'status' => 'ok',
                'message' => 'Le service a été créé',
            ],
            $this->s2lowClient->createAuthorityService(123, 'Root')
        );
    }

    /**
     * @throws ClientExceptionInterface
     * @throws S2lowClientException
     */
    public function testAddUserToService()
    {
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->willReturn(
                new Response(
                    200,
                    [],
                    json_encode([

                        'status' => 'ok',
                        'message' => "L'utilisateur a été ajouté au service",
                    ])
                )
            );
        $this->assertEquals(
            [
                'status' => 'ok',
                'message' => "L'utilisateur a été ajouté au service",
            ],
            $this->s2lowClient->addUserToService(123, 1)
        );
    }

    /**
     * @throws ClientExceptionInterface
     */
    public function testAddSirenToGroup(): void
    {
        $this->httpClient->expects($this->once())
            ->method('sendRequest')
            ->willReturn(new Response(301));

        $this->assertTrue(
            $this->s2lowClient->addSirenToGroup(1, '000000000')
        );
    }
}
