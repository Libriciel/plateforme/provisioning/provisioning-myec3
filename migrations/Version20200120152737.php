<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200120152737 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(!in_array($this->connection->getDatabasePlatform()->getName(), ['sqlite', 'mysql']), 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('CREATE TABLE product_user_socle (product_id VARCHAR(255) NOT NULL, user_socle_id VARCHAR(255) NOT NULL, user_product_id VARCHAR(255) NOT NULL, PRIMARY KEY(product_id, user_socle_id))');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf(!in_array($this->connection->getDatabasePlatform()->getName(), ['sqlite', 'mysql']), 'Migration can only be executed safely on \'sqlite\'.');

        $this->addSql('DROP TABLE product_user_socle');
    }
}
