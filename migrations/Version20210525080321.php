<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210525080321 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE product_department_socle DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE product_department_socle ADD product_number INT NOT NULL DEFAULT 1;');
        $this->addSql('ALTER TABLE product_department_socle ADD PRIMARY KEY (product_id, department_socle_id, product_number);');
        $this->addSql('ALTER TABLE product_organization_socle DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE product_organization_socle ADD product_number INT NOT NULL DEFAULT 1;');
        $this->addSql('ALTER TABLE product_organization_socle ADD PRIMARY KEY (product_id, organization_socle_id, product_number);');
        $this->addSql('ALTER TABLE product_user_socle DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE product_user_socle ADD product_number INT NOT NULL DEFAULT 1;');
        $this->addSql('ALTER TABLE product_user_socle ADD PRIMARY KEY (product_id, user_socle_id, product_number);');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE product_department_socle DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE product_department_socle DROP product_number;');
        $this->addSql('ALTER TABLE product_department_socle ADD PRIMARY KEY (product_id, department_socle_id);');
        $this->addSql('ALTER TABLE product_organization_socle DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE product_organization_socle DROP product_number;');
        $this->addSql('ALTER TABLE product_organization_socle ADD PRIMARY KEY (product_id, organization_socle_id);');
        $this->addSql('ALTER TABLE product_user_socle DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE product_user_socle DROP product_number;');
        $this->addSql('ALTER TABLE product_user_socle ADD PRIMARY KEY (product_id, user_socle_id);');
    }
}
